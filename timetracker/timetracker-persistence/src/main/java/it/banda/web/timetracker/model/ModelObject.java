package it.banda.web.timetracker.model;

import java.io.Serializable;

/**
 * An empty interface needed to simplify DAO pattern with Generics. It helps to
 * abstract away the boilerpate CRUD operation, which would be otherwise repeated
 * over and over again by each DAO class in this application.
 * 
 * @author a.ferreira
 * @since 08/06/2017
 */
public interface ModelObject extends Serializable {

}
