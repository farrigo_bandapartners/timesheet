package it.banda.web.timetracker.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.banda.web.timetracker.dao.RoleDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Role;

/**
 * @author a.ferreira
 * @since 13/06/2017
 */

@Repository("roleDao")
public class RoleDaoImpl extends GenericDaoImpl<Role> implements RoleDao {
	private static final Logger logger = LoggerFactory.getLogger(RoleDaoImpl.class);
	
	public RoleDaoImpl() {
		super(Role.class);
	}

	@Override
	@Transactional(readOnly = true)
	public Role findRole(String name) throws EntityNotFoundException {
		logger.info("Finding Role record from DB with the name '" + name + "' ...");
		
		Query query = entityManager.createQuery("select ro from Role ro where ro.roleName = :roleName");
		query.setParameter("roleName", name);
		
		Role role = null;
		try {
			role = (Role) query.getSingleResult();
		} catch (NoResultException ex) {
			logger.error("No Role found forn name: '" + name + "'");
			throw new EntityNotFoundException("No Role found forn name: '" + name + "'", ex);
		}
		
		logger.info("Role '" + name + "' found successfully.");
		return role;
	}

	@Override
	public Role findByIdFetchUsers(Long roleId) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Role> findRoleListFetchUsers(Long roleId) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
