package it.banda.web.timetracker.dao;

import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Activity;

import java.util.List;

/**
 * @author a.ferreira
 * @since 06/06/2017
 */

public interface ActivityDao extends GenericDao<Activity> {
	public Activity findActivity(String name) throws EntityNotFoundException;
	public Activity findByIdFetchAllocations(Long activityId) throws EntityNotFoundException;
	public List<Activity> findActivityListFetchAllocations(Long activityId);
	public void deleteCascade(Activity activity);
}
