package it.banda.web.timetracker.dao;

import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Allocation;

import java.util.List;


/**
 * @author a.ferreira
 * @since 10/06/2017
 */

public interface AllocationDao extends GenericDao<Allocation> {
	public Allocation findAllocation(String name) throws EntityNotFoundException;
	public Allocation findByIdFetchDays(Long allocationId) throws EntityNotFoundException;
	public List<Allocation> findAllocationListFetchDays();
	//public void deleteAllocationCascade(Allocation allocation);
}
