package it.banda.web.timetracker.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.banda.web.timetracker.dao.PeriodDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Period;

/**
 * 
 * @author farrigo
 *
 */
@Repository("periodDao")
public class PeriodDaoImpl extends GenericDaoImpl<Period> implements PeriodDao {
	
	private static final Logger logger = LoggerFactory.getLogger(PeriodDaoImpl.class);
	
	public PeriodDaoImpl() {
		super(Period.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Period> findAllPeriodsByUserId(Long userId) throws EntityNotFoundException {
		logger.debug("Getting periods for userId " + userId);
		Query query = entityManager.createQuery("select pe from Period pe where pe.id = :id");
		query.setParameter("id", userId);
		return query.getResultList();
	}

}
