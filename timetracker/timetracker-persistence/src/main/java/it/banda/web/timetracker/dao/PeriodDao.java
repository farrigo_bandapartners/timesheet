package it.banda.web.timetracker.dao;

import java.util.List;

import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Period;

/**
 * 
 * @author farrigo
 *
 */
public interface PeriodDao extends GenericDao<Period> {
	public List<Period> findAllPeriodsByUserId(Long userId) throws EntityNotFoundException;
}
