package it.banda.web.timetracker.service;

import java.util.List;

import it.banda.web.timetracker.dao.RoleDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author a.ferreira
 * @since 13/06/2017
 */

@Service("roleService")
public class RoleService implements RoleDao {
	
	@Autowired
	private RoleDao roleDao;

	@Override
	public Role findById(Long id) {
		return roleDao.findById(id);
	}

	@Override
	public List<Role> findAll() {
		return roleDao.findAll();
	}

	@Override
	public void save(Role role) {
		roleDao.save(role);
	}

	@Override
	public void update(Role role) {
		roleDao.update(role);
	}

	@Override
	public void delete(Role role) {
		roleDao.delete(role);
	}

	@Override
	public Role findRole(String name) throws EntityNotFoundException {
		return roleDao.findRole(name);
	}

	@Override
	public Role findByIdFetchUsers(Long roleId) throws EntityNotFoundException {
		return roleDao.findByIdFetchUsers(roleId);
	}

	@Override
	public List<Role> findRoleListFetchUsers(Long roleId) {
		return roleDao.findRoleListFetchUsers(roleId);
	}
	
}
