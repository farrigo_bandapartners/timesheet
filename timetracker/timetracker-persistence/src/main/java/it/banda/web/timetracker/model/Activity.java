package it.banda.web.timetracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.banda.web.timetracker.enumeration.ActivityType;

/**
 * @author a.ferreira
 * @since 06/06/2017
 */

@Entity
@Table(name = "T_ACTIVITY")
public class Activity implements ModelObject {
	private static final long serialVersionUID = -7415619902671298636L;
	
	private Long id;
	private String code;
	private String description;
	private String customer;
	private String refCustomer;
	private String refCustomerEmail;
	private String refCustomerPhone;
	private String note;
	private ActivityType type;
	private Integer version;
	
	public Activity() {
	}
	
	public Activity(Long id, String name, String description, ActivityType type) {
		this.id = id;
		this.description = description;
		this.type = type;
	}

	@Id
	@Column(name = "activity_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Enumerated(EnumType.STRING)
	public ActivityType getType() {
		return type;
	}

	public void setType(ActivityType type) {
		this.type = type;
	}
	
	@Column(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "customer")
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	@Column(name = "ref_customer")
	public String getRefCustomer() {
		return refCustomer;
	}

	public void setRefCustomer(String refCustomer) {
		this.refCustomer = refCustomer;
	}

	@Column(name = "ref_customer_email")
	public String getRefCustomerEmail() {
		return refCustomerEmail;
	}

	public void setRefCustomerEmail(String refCustomerEmail) {
		this.refCustomerEmail = refCustomerEmail;
	}

	@Column(name = "ref_customer_phone")
	public String getRefCustomerPhone() {
		return refCustomerPhone;
	}

	public void setRefCustomerPhone(String refCustomerPhone) {
		this.refCustomerPhone = refCustomerPhone;
	}
	
	@Column(name = "note")
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}

	@JsonIgnore
	@Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
