package it.banda.web.timetracker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.banda.web.timetracker.dao.PeriodDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.Period;

/**
 * @author a.ferreira
 * @since 10/06/2017
 */

@Service("periodService")
public class PeriodService implements PeriodDao {
	
	@Autowired
	private PeriodDao periodDao;
	
	@Override
	public Period findById(Long id) {
		return periodDao.findById(id);
	}

	@Override
	public List<Period> findAllPeriodsByUserId(Long userId) throws EntityNotFoundException {
		return periodDao.findAllPeriodsByUserId(userId);
	}

	@Override
	public List<Period> findAll() {
		return periodDao.findAll();
	}

	@Override
	public void save(Period object) {
		periodDao.save(object);
	}

	@Override
	public void update(Period object) {
		periodDao.update(object);
	}

	@Override
	public void delete(Period object) {
		periodDao.delete(object);
	}

}
