package it.banda.web.timetracker.dao;

import java.util.List;

import it.banda.web.timetracker.model.ModelObject;

/**
 * 
 * @author a.ferreira
 * @since 08/06/2017
 *
 * @param <T> The actual model class instantiated
 */
public interface GenericDao<T extends ModelObject> {
	public T findById(Long id);
	public List<T> findAll();
	public void save(T object);
	public void update(T object);
	public void delete(T object);
}
