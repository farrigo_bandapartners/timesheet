package it.banda.web.timetracker.dao.impl;

import it.banda.web.timetracker.dao.UserDao;
import it.banda.web.timetracker.exception.EntityNotFoundException;
import it.banda.web.timetracker.model.User;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

/**
 * @author a.ferreira
 * @since 08/06/2017
 */

@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {
	private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	
	public UserDaoImpl() {
		super(User.class);
	}

	@Override
	public User findUser(String username) throws DataAccessException, EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User authenticateUser(String username, String password) throws DataAccessException, EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findByIdFetchAllocations(Long userId) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findUserListFetchAllocations() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
