package it.banda.web.timetracker.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.banda.web.timetracker.model.Allocation;
import it.banda.web.timetracker.service.AllocationService;
import it.banda.web.timetracker.service.RoleService;
import it.banda.web.timetracker.service.UserService;

/**
 * 
 * @author farrigo
 *
 */
@RestController
public class AllocationController {
	private static final Logger logger = LoggerFactory.getLogger(AllocationController.class);
	
//	@Autowired
//	private ActivityService activityService;
	
	@Autowired
	private AllocationService allocationService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "/rest/entry/allocations", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> addAllocation(@RequestBody Allocation allocation) {
		logger.info("Creating a new Activity...");
		
		if (allocation == null) {
			logger.error("The Allocation to save cannot be null!");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		try {
			allocationService.save(allocation);
		} catch (Exception ex) {
			logger.error("Error on saving the new Allocation!", ex);
			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
		}
		
		logger.info("New Allocation created successfully.");
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/rest/entry/allocations/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Allocation> getAllocation(@PathVariable Long id) {
		logger.info("Retrieving the Activity with ID '" + id + "' from DB...");
		
		Allocation allocation = null;
		try {
			allocation = allocationService.findById(id);
//		} catch (EntityNotFoundException ex) {
		} catch (Exception ex) {
			logger.error("No Allocation found for ID: " + id);
			return new ResponseEntity<Allocation>(HttpStatus.NOT_FOUND);
		}
		
		logger.info("Allocation with ID '" + id + "' found successfully.");
		return new ResponseEntity<Allocation>(allocation, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rest/entry/allocations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Allocation>> listAllAllocations() {
		logger.info("Retrieving the list of all Allocations from DB...");
		
		List<Allocation> allocations = allocationService.findAll();
		if (allocations == null || allocations.isEmpty()) {
			logger.debug("No allocation fround!");
			return new ResponseEntity<List<Allocation>>(HttpStatus.NO_CONTENT);
		}
		
		logger.info("It was found a total of " + allocations.size() + " allocation/allocations.");
		return new ResponseEntity<List<Allocation>>(allocations, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/rest/entry/allocations/{id}", method = RequestMethod.PUT, 
//			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Activity> updateActivity(@PathVariable Long id, @RequestBody Activity updatedActivity) {
//		logger.info("Updating the Activity with ID '" + id + "' ...");
//		
//		Activity activity = null;
//		try {
//			activity = activityService.findByIdFetchAllocations(id);
//			activity.setName(updatedActivity.getName());
//			activity.setDescription(updatedActivity.getDescription());
//			activity.setType(updatedActivity.getType());
//			
//			if (null != updatedActivity.getAllocations() && updatedActivity.getAllocations().size() > 0) {
//				activity.setAllocations(updatedActivity.getAllocations());
//			}
//			
//			activityService.update(activity);
//		} catch (EntityNotFoundException ex) {
//			logger.error("No Activity found for ID: " + id);
//			return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
//		} catch (Exception ex) {
//			logger.error("Error on updating the new Activity!", ex);
//			return new ResponseEntity<Activity>(HttpStatus.BAD_GATEWAY);
//		}
//		
//		logger.info("Activity updated successfully.");
//		return new ResponseEntity<Activity>(activity, HttpStatus.OK);
//	}
//	
//	@RequestMapping(value = "/rest/entry/allocations/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<Void> deleteActivity(@PathVariable Long id) {
//		logger.debug("Deleting the Activity with ID '" + id + "'");
//		
//		Activity activity = null;
//		try {
//			activity = activityService.findById(id);
//			activityService.deleteCascade(activity);
//		} catch (NoResultException ex) {
//			logger.error("No Activity found for ID: " + id);
//			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
//		} catch (Exception ex) {
//			logger.error("Error on deleting the Activity!", ex);
//			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
//		}
//		
//		logger.info("Activity deleted successfully.");
//		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
//	}
	
//	@RequestMapping(value = "/activity/list/dto/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<ActivityListDTO> getActivityList(@PathVariable Long id) {
//		logger.info("Retrieving the Activity List with ID '" + id + "' ...");
//		
//		Activity activity = null;
//		ActivityListDTO activityDTOList = new ActivityListDTO();
//		try {
//			activity= activityService.findByIdFetchAllocations(id);
//			
//			if (activity == null) {
//				logger.debug("No activity fround!");
//				return new ResponseEntity<ActivityListDTO>(HttpStatus.NO_CONTENT);
//			}
//			
//			for (Allocation allocation : activity.getAllocations()) {
//				Allocation allocForDTO = allocationService.findByIdFetchDays(allocation.getId());
//				// TODO ...
//			}
//			
//		} catch (EntityNotFoundException ex) {
//			
//		}
//		
//		//logger.info("It was found a total of " + activities.size() + " activity/activities.");
//		return new ResponseEntity<ActivityListDTO>(activityDTOList, HttpStatus.OK);
//	}
	
}
