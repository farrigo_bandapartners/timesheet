package it.banda.web.timetracker.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.banda.web.timetracker.model.Period;
import it.banda.web.timetracker.service.PeriodService;

/**
 * 
 * @author farrigo
 *
 */
@RestController
public class PeriodController {
	private static final Logger logger = LoggerFactory.getLogger(PeriodController.class);
	
	@Autowired
	private PeriodService periodService;
	
//	@Autowired
//	private AllocationService allocationService;
//	
//	@Autowired
//	private UserService userService;
//	
//	@Autowired
//	private RoleService roleService;
	
	@RequestMapping(value = "/rest/entry/periods", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> addPeriod(@RequestBody Period period) {
		logger.info("Creating a new Period...");
		
		if (period == null) {
			logger.error("The Period to save cannot be null!");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		try {
			periodService.save(period);
		} catch (Exception ex) {
			logger.error("Error on saving the new Period!", ex);
			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
		}
		
		logger.info("New Period created successfully.");
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/rest/entry/periods/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Period> getPeriod(@PathVariable Long id) {
		logger.info("Retrieving the Period with ID '" + id + "' from DB...");
		
		Period period = null;
		try {
			period = periodService.findById(id);
		} catch (EntityNotFoundException ex) {
			logger.error("No Period found for ID: " + id);
			return new ResponseEntity<Period>(HttpStatus.NOT_FOUND);
		}
		
		logger.info("Period with ID '" + id + "' found successfully.");
		return new ResponseEntity<Period>(period, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rest/entry/periods", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Period>> listAllActivities(@RequestParam("userId") String userId) {
		logger.info("Retrieving the list of all Periods from DB...");
		
		List<Period> periods;
		try {
			if (userId != null && !userId.trim().equals("")) {
				periods = periodService.findAllPeriodsByUserId(Long.parseLong(userId));
			}
			else {
				periods = periodService.findAll();
			}
			
			if (periods == null || periods.isEmpty()) {
				logger.debug("No periods fround for user ID: " + userId + "!");
				return new ResponseEntity<List<Period>>(HttpStatus.NO_CONTENT);
			}
		} catch (it.banda.web.timetracker.exception.EntityNotFoundException e) {
			logger.error("No periods fround for user ID: " + userId + "!");
			return new ResponseEntity<List<Period>>(HttpStatus.NOT_FOUND);
		}
		
		logger.info("It was found a total of " + periods.size() + " period/periods.");
		return new ResponseEntity<List<Period>>(periods, HttpStatus.OK);
	}
	
}
