package it.banda.web.timetracker.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.banda.web.timetracker.model.User;
import it.banda.web.timetracker.service.RoleService;
import it.banda.web.timetracker.service.UserService;

/**
 * 
 * @author farrigo
 *
 */
@RestController
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
//	@Autowired
//	private ActivityService activityService;
	
//	@Autowired
//	private AllocationService allocationService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value = "/rest/entry/resources", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> addResource(@RequestBody User resource) {
		logger.info("Creating a new Activity...");
		
		if (resource == null) {
			logger.error("The Resource to save cannot be null!");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		try {
			userService.save(resource);
		} catch (Exception ex) {
			logger.error("Error on saving the new resource!", ex);
			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
		}
		
		logger.info("New Resource created successfully.");
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/rest/entry/resources/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> getResource(@PathVariable Long id) {
		logger.info("Retrieving the Resource with ID '" + id + "' from DB...");
		
		User resource = null;
		try {
			resource = userService.findById(id);
//		} catch (EntityNotFoundException ex) {
		} catch (Exception ex) {
			logger.error("No resource found for ID: " + id);
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		
		logger.info("Resource with ID '" + id + "' found successfully.");
		return new ResponseEntity<User>(resource, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/rest/entry/resources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> listAllResources() {
		logger.info("Retrieving the list of all Activities from DB...");
		
		List<User> resources = userService.findAll();
		if (resources == null || resources.isEmpty()) {
			logger.debug("No Resource found!");
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		
		logger.info("It was found a total of " + resources.size() + " resource/resources.");
		return new ResponseEntity<List<User>>(resources, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/rest/entry/allocations/{id}", method = RequestMethod.PUT, 
//			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Activity> updateActivity(@PathVariable Long id, @RequestBody Activity updatedActivity) {
//		logger.info("Updating the Activity with ID '" + id + "' ...");
//		
//		Activity activity = null;
//		try {
//			activity = activityService.findByIdFetchAllocations(id);
//			activity.setName(updatedActivity.getName());
//			activity.setDescription(updatedActivity.getDescription());
//			activity.setType(updatedActivity.getType());
//			
//			if (null != updatedActivity.getAllocations() && updatedActivity.getAllocations().size() > 0) {
//				activity.setAllocations(updatedActivity.getAllocations());
//			}
//			
//			activityService.update(activity);
//		} catch (EntityNotFoundException ex) {
//			logger.error("No Activity found for ID: " + id);
//			return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
//		} catch (Exception ex) {
//			logger.error("Error on updating the new Activity!", ex);
//			return new ResponseEntity<Activity>(HttpStatus.BAD_GATEWAY);
//		}
//		
//		logger.info("Activity updated successfully.");
//		return new ResponseEntity<Activity>(activity, HttpStatus.OK);
//	}
//	
//	@RequestMapping(value = "/rest/entry/allocations/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<Void> deleteActivity(@PathVariable Long id) {
//		logger.debug("Deleting the Activity with ID '" + id + "'");
//		
//		Activity activity = null;
//		try {
//			activity = activityService.findById(id);
//			activityService.deleteCascade(activity);
//		} catch (NoResultException ex) {
//			logger.error("No Activity found for ID: " + id);
//			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
//		} catch (Exception ex) {
//			logger.error("Error on deleting the Activity!", ex);
//			return new ResponseEntity<Void>(HttpStatus.BAD_GATEWAY);
//		}
//		
//		logger.info("Activity deleted successfully.");
//		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
//	}
	
//	@RequestMapping(value = "/activity/list/dto/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<ActivityListDTO> getActivityList(@PathVariable Long id) {
//		logger.info("Retrieving the Activity List with ID '" + id + "' ...");
//		
//		Activity activity = null;
//		ActivityListDTO activityDTOList = new ActivityListDTO();
//		try {
//			activity= activityService.findByIdFetchAllocations(id);
//			
//			if (activity == null) {
//				logger.debug("No activity fround!");
//				return new ResponseEntity<ActivityListDTO>(HttpStatus.NO_CONTENT);
//			}
//			
//			for (Allocation allocation : activity.getAllocations()) {
//				Allocation allocForDTO = allocationService.findByIdFetchDays(allocation.getId());
//				// TODO ...
//			}
//			
//		} catch (EntityNotFoundException ex) {
//			
//		}
//		
//		//logger.info("It was found a total of " + activities.size() + " activity/activities.");
//		return new ResponseEntity<ActivityListDTO>(activityDTOList, HttpStatus.OK);
//	}
	
}
