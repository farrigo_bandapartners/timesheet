package it.banda.web.timetracker.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.banda.web.timetracker.model.MenuItem;

/**
 * 
 * @author farrigo
 *
 */
@RestController
public class MenuItemController {
	private static final Logger logger = LoggerFactory.getLogger(MenuItemController.class);
	
	private static final String SEARCH_TIMESHEET = "search-timesheet";
	private static final String NAVIGATION = "navigation";
	private static final String INSERT_ALLOCATION = "insert-allocation";
	private static final String SEARCH_ALLOCATION = "search-allocation";
	private static final String INSERT_RESOURCE = "insert-resource";
	private static final String SEARCH_RESOURCE = "search-resource";
	private static final String INSERT_ORDER = "insert-order";
	private static final String SEARCH_ORDER = "search-order";
	
	
//	@Autowired
//	private ActivityService activityService;
//	
//	@Autowired
//	private AllocationService allocationService;
//	
//	@Autowired
//	private UserService userService;
//	
//	@Autowired
//	private RoleService roleService;
	
	@RequestMapping(value = "/rest/entry/menuItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<MenuItem>> getAllMenuItems() {
		
		logger.info("Retrieving the list of all menu items...");
		
		List<MenuItem> menuItemList = new ArrayList<MenuItem>();
		
		MenuItem menuItem_1 = new MenuItem();
		menuItem_1.setId("0");
		menuItem_1.setLabel("RILEVAZIONE ORE");
		menuItem_1.setLevel("0");
		menuItem_1.setType(SEARCH_TIMESHEET);
		menuItem_1.setValue("0");
		menuItemList.add(menuItem_1);
		
		MenuItem menuItem_2 = new MenuItem();
		menuItem_2.setId("1");
		menuItem_2.setLabel("ALLOCAZIONE RISORSE");
		menuItem_2.setLevel("0");
		menuItem_2.setType(NAVIGATION);
		menuItem_2.setValue("1");
		menuItemList.add(menuItem_2);
		
		MenuItem menuItem_3 = new MenuItem();
		menuItem_3.setId("2");
		menuItem_3.setLabel("INSERIMENTO");
		menuItem_3.setLevel("1");
		menuItem_3.setType(INSERT_ALLOCATION);
		menuItem_3.setValue("2");
		menuItemList.add(menuItem_3);
		
		MenuItem menuItem_4 = new MenuItem();
		menuItem_4.setId("3");
		menuItem_4.setLabel("RICERCA-MODIFICA");
		menuItem_4.setLevel("1");
		menuItem_4.setType(SEARCH_ALLOCATION);
		menuItem_4.setValue("3");
		menuItemList.add(menuItem_4);
		
		MenuItem menuItem_5 = new MenuItem();
		menuItem_5.setId("4");
		menuItem_5.setLabel("GESTIONE RISORSE");
		menuItem_5.setLevel("0");
		menuItem_5.setType(NAVIGATION);
		menuItem_5.setValue("4");
		menuItemList.add(menuItem_5);
		
		MenuItem menuItem_6 = new MenuItem();
		menuItem_6.setId("5");
		menuItem_6.setLabel("INSERIMENTO");
		menuItem_6.setLevel("1");
		menuItem_6.setType(INSERT_RESOURCE);
		menuItem_6.setValue("5");
		menuItemList.add(menuItem_6);
		
		MenuItem menuItem_7 = new MenuItem();
		menuItem_7.setId("6");
		menuItem_7.setLabel("RICERCA-MODIFICA");
		menuItem_7.setLevel("1");
		menuItem_7.setType(SEARCH_RESOURCE);
		menuItem_7.setValue("6");
		menuItemList.add(menuItem_7);
		
		MenuItem menuItem_8 = new MenuItem();
		menuItem_8.setId("7");
		menuItem_8.setLabel("GESTIONE CONTRATTI");
		menuItem_8.setLevel("0");
		menuItem_8.setType(NAVIGATION);
		menuItem_8.setValue("7");
		menuItemList.add(menuItem_8);
		
		MenuItem menuItem_9 = new MenuItem();
		menuItem_9.setId("8");
		menuItem_9.setLabel("INSERIMENTO");
		menuItem_9.setLevel("1");
		menuItem_9.setType(INSERT_ORDER);
		menuItem_9.setValue("8");
		menuItemList.add(menuItem_9);
		
		MenuItem menuItem_10 = new MenuItem();
		menuItem_10.setId("9");
		menuItem_10.setLabel("RICERCA-MODIFICA");
		menuItem_10.setLevel("1");
		menuItem_10.setType(SEARCH_ORDER);
		menuItem_10.setValue("9");
		menuItemList.add(menuItem_10);
		
		MenuItem menuItem_11 = new MenuItem();
		menuItem_11.setId("10");
		menuItem_11.setLabel("HOME PAGE");
		menuItem_11.setLevel("0");
		menuItem_11.setType(null);
		menuItem_11.setValue("10");
		menuItemList.add(menuItem_11);
		
//		if (activities == null || activities.isEmpty()) {
//			logger.debug("No activity fround!");
//			return new ResponseEntity<List<Activity>>(HttpStatus.NO_CONTENT);
//		}
		
		logger.info("It was found a total of " + menuItemList.size() + " menuItems.");
		return new ResponseEntity<List<MenuItem>>(menuItemList, HttpStatus.OK);
	}
	
}
