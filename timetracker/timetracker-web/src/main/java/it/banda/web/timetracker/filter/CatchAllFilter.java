package it.banda.web.timetracker.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.ConfigurationException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author agsuser
 *
 */
public class CatchAllFilter implements Filter {
	
	private final static Logger logger = LoggerFactory.getLogger(CatchAllFilter.class);
	
	public final static String SESSION_TYPE_ATTR_NAME = "SESSION_TYPE";
	public final static String CONTEXT_SESSION = "CONTEXT_SESSION";
	public final static String CONTEXT_JOB_ID = "CONTEXT_JOB_ID";
	public final static String USERNAME_ATTR_NAME = "username";
	public final static String MAIN_SESSION = "MAIN_SESSION";
	
	private List<String> enabledResources;
	private String loginPage;
	private String loginUrl;
	private String homePage;
	private String context;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.enabledResources = new ArrayList<String>();
		this.context = filterConfig.getServletContext().getContextPath();
		String resourceString = filterConfig.getInitParameter("enabled-resources");
		String[] resources = resourceString.split(",");
		for (String resource : resources) {
			this.enabledResources.add(this.context + resource);
		}
		this.loginPage = filterConfig.getInitParameter("login-page");
		this.loginUrl = filterConfig.getInitParameter("login-url");
		this.homePage = filterConfig.getInitParameter("home-page");
        try {
        	filterConfig.getServletContext().setAttribute("login-page", this.loginPage);
        	filterConfig.getServletContext().setAttribute("home-page", this.homePage);
        }
        catch (Exception e) {
        	e.printStackTrace();     
        }
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		
		HttpSession session = req.getSession(false);
		
		String usr = req.getParameter("username") + "";
		String pwd = req.getParameter("password") + "";
		
		String reqUri = req.getRequestURI();
		
		if (session == null) {
			if (!isEnabledResource(reqUri)) {
				if (reqUri != null && reqUri.equals(this.context + "/" + this.loginUrl)) {
					boolean isValidUser = false;
					boolean loginError = false;
					try {
						isValidUser = isAuthorizedUser(req, usr, pwd);
					} 
					catch (ConfigurationException e) {
						e.printStackTrace();
						loginError = true;
					}
					catch (IOException e) {
						e.printStackTrace();
						loginError = true;
					}
					catch (ServletException e) {
						e.printStackTrace();
						loginError = true;
					}
					catch (Throwable e) {
						e.printStackTrace();
						loginError = true;
					}
					if (loginError) {
						RequestDispatcher dispatcher = request.getRequestDispatcher(this.loginPage);
						request.setAttribute("errorMsg", "Errore nel servizio di Login");
						dispatcher.forward(request, response);
					}
					else if (isValidUser) {
						res.sendRedirect(this.homePage);
					}
					else {
						RequestDispatcher dispatcher = request.getRequestDispatcher(this.loginPage);
						request.setAttribute("errorMsg", "Username o Password non corretti");
						dispatcher.forward(request, response);
					}
				}
				else if (reqUri != null && (reqUri.endsWith(".jsp") || reqUri.equals(this.context + "/logout"))) {
					res.sendRedirect(this.loginPage);
				}
				// Per le altre risorse non autorizzate di default, ad es. le chiamate
				// ai servizi REST, si ritorna un JSON d'errore che dovra' essere gestito
				// dalle chiamate dei model backbone.
				else {
					RequestDispatcher dispatcher = request.getRequestDispatcher(this.loginPage);
					request.setAttribute("errorMsg", "Autenticazione non valida.");
					dispatcher.forward(request, response);
					
					// TODO
//					PrintWriter out = res.getWriter();
//					out.write("{\"result\": {\"error\": \"1\", \"code\": \"440\", \"message\": \"sessione scaduta o non valida.\"} }");
//					out.flush();
//					out.close();
				}
			}
			else {
				chain.doFilter(request, response);
			}
		}
		// altrimenti la sessione è già istanziata...
		else if (reqUri != null && reqUri.equals(this.context + "/" + this.loginUrl)) {
			res.sendRedirect(this.homePage);
		}
		else if (reqUri != null && reqUri.equals(this.context + "/logout")) {
			logger.info("logout by user: " + session.getAttribute("username"));
			logout(req, (String)session.getAttribute(USERNAME_ATTR_NAME));
			res.sendRedirect(this.homePage);
		}
		else {
			chain.doFilter(request, response);
			// Aggiunta delle direttive di no-cache per evitare, dopo il logout, di ritornare
			// sulla pagina dell'utente (home.jsp) usando il tasto 'back' del browser.
			if ((reqUri.equals(this.context + "/" + this.homePage))) {
				((HttpServletResponse)response).setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
				((HttpServletResponse)response).setHeader("Pragma", "no-cache"); // HTTP 1.0.
				((HttpServletResponse)response).setDateHeader("Last-Modified", (new Date()).getTime() - 31L*24L*60L*60L*1000L); // Proxies: set last modified to right now.
			}
		}
	}
	
	/**
	 * @param request
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws ConfigurationException
	 */
	private boolean isAuthorizedUser(HttpServletRequest request, String username, String password) throws IOException, ServletException, ConfigurationException {
		boolean retVal = false;
        HttpSession session = request.getSession(true);
        session.setAttribute(USERNAME_ATTR_NAME, username);
        session.setAttribute(SESSION_TYPE_ATTR_NAME, MAIN_SESSION);
        retVal = true;
        return retVal;
    }
	
	@Override
	public void destroy() {
		//
	}
	
	/**
	 * @param request
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws ConfigurationException
	 */
	private boolean logout(HttpServletRequest request, String username) throws IOException, ServletException {
		boolean retVal = false;
		logger.info("logout by user: " + httpUrlncode(username));
        HttpSession session = request.getSession(false);
        if (session != null) {
        	session.invalidate();
        }
        retVal = true;
        return retVal;
    }
	
	/**
	 * risorse http abilitate anche senza cookie di sessione
	 * 
	 * @param resource
	 * @return
	 */
	private boolean isEnabledResource(String resource) {
		for (String res : this.enabledResources) {
			if (res != null && res.endsWith("*") && res.trim().length() > 1) {
				if (resource != null && resource.startsWith(res.substring(0, res.length()-2))) {
					return true;
				}
			}
			else {
				if (resource != null && resource.equals(res)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param chunk
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String httpUrlncode(String chunk) throws UnsupportedEncodingException {
		return URLEncoder.encode(chunk, "UTF-8");
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
//	private String getRequestType(HttpServletRequest request) {
//		String actualRequestType = null;
//		String reqUri = request.getRequestURI();
//		actualRequestType = (reqUri.indexOf(this.contextHomePage) > 0) ? CONTEXT_SESSION : 
//													((reqUri.indexOf(this.homePage) > 0) ? MAIN_SESSION : null);
//		return actualRequestType;
//	}

}
