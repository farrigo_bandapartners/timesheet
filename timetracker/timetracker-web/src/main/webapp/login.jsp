<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>Timetracker Login</title>
	
	<link rel="stylesheet" type="text/css" href="./css/gui.css" />
	<link rel="stylesheet" type="text/css" href="./css/fmwk-login.css" />
	
</head>
<body>

<!-- 	<div class="page_header_delim">&nbsp;</div> -->
	
	<div class="align-vertically">
	
		<div class="align-horizontally">
			<div class="aligned-element">
				<form method="post" action="login">
					<table class="login_table">
						<tbody>
							<tr>
								<td><span class="text">Username</span></td>
								<td style="text-align: right;"><input type="text" name="username" class="textBox"/></td>
					        </tr>
							<tr>
								<td><span class="text">Password</span></td>
								<td style="text-align: right;"><input type="password" name="password" class="textBox"/></td>
							</tr>
					        <tr>
					            <td colspan="2" style="text-align: right;">
					                <input class="btn-fmwk-login" type="submit" value="Invia"/>
					                <input class="btn-fmwk-login" type="reset" value="Annulla"/>
					            </td>
					        </tr>
					        <tr>
					            <td colspan="2" style="text-align: right; color: red; font-size: 12px;">
							        <c:if test="${ not empty errorMsg and fn:trim(errorMsg) != '' }">${ errorMsg }</c:if>
					            </td>
					        </tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	
	</div>
	
</body>
</html>

