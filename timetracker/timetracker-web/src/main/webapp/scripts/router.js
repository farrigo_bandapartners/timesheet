define([
	'jquery',
	'underscore',
	'backbone',
	'views/homeView'
], 
function ($, _, Backbone, HomeView, ContextHomeView) {
	
	var AppRouter = Backbone.Router.extend({
		
		routes: {
			'' 									: 'showDefault'
//				,
//			'contextHome/:migTypeId/:jobId'		: 'showContextHome',
//			'contextHome/:migTypeId'			: 'showContextHome'
		},
		
		homeView : null,
		contextHomeView : null,
		
		// Il default è la 'homeView'
		showDefault: function () {
			if (!this.homeView) {
				var homeView = new HomeView();
				this.homeView = homeView;
			}
			this.homeView.render();
		}
		
//		,
//		
//		showContextHome: function (migTypeId, jobId) {
//			console.log('migrationTypeId: ' + migTypeId + ', jobId: ' + jobId);
//			if (!this.contextHomeView) {
//				var contextHomeView = new ContextHomeView();
//				this.contextHomeView = contextHomeView;
//			}
//			this.contextHomeView.render(migTypeId, jobId);
//		}
		
	});
		
	var initialize = function () {
			
		var appRouter = new AppRouter();
		Backbone.history.start();

	};
		
	return {
		'initialize': initialize
	};
	
});
