
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var SavedTimesheetHeaderList = Backbone.Collection.extend({

		url: engineRestServiceUrl + '/timesheetHeaders',
		
	});
	
	return SavedTimesheetHeaderList;
	
});