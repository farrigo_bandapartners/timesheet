
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var JobList = Backbone.Collection.extend({

		url: engineRestServiceUrl + '/activities'
		
	});
	
	return JobList;
	
});