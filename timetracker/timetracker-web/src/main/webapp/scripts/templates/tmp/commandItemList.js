
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var CommandList = Backbone.Collection.extend({

		url: engineRestServiceUrl + '/commandItems',
		
	});
	
	return CommandList;
	
});