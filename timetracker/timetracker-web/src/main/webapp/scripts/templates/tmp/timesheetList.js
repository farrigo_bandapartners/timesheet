
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var SavedTimesheetItemList = Backbone.Collection.extend({

		url: engineRestServiceUrl + '/timesheets',
		
	});
	
	return SavedTimesheetItemList;
	
});