
define([
	'jquery',
	'underscore',
	'backbone',
	'../models/jobCommand'
], function ($, _, Backbone, LoadJobCommand) {
	
	// Classe utilizzata per l'invio della richiesta d'esecuzione 
	// di un comando su di un cluster o su una lista di job.
	var JobCommandList = Backbone.Collection.extend({
		
		model: LoadJobCommand,

		url: engineRestServiceUrl + '/jobCommands'
	
	});
	
	return JobCommandList;
	
});
