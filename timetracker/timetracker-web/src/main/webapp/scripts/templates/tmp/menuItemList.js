
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var MenuItemList = Backbone.Collection.extend({

		url: engineRestServiceUrl + '/menuItems'
		
	});
	
	return MenuItemList;
	
});