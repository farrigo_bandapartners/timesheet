
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var TabList = Backbone.Collection.extend({

		url: engineRestServiceUrl + '/tabs'
		
	});
	
	return TabList;
	
});