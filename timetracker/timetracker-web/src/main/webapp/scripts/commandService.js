
define([
	'jquery',
	'underscore',
	'backbone',
	'./models/file',
	'./utils'
], function ($, _, Backbone, File, Utils) {
	
	var attrs = null;
	
	var CommandService = $.fn.extend({
		
		sendGetXlsCommand: function(scope, cmdEndpoint, downloadEndpoint, qStringParameters) {
			this.sendGetXlsCommandInternal.call(scope, cmdEndpoint, downloadEndpoint, qStringParameters);
		},
		
		sendGetXlsCommandInternal: function(cmdEndpoint, downloadEndpoint, qStringParameters) {
			var file = new File();
			if (!downloadEndpoint || downloadEndpoint.length == 0) {
				Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non valida.');
			}
			if (cmdEndpoint && cmdEndpoint != '') {
				file.url = engineRestServiceUrl + '/' + cmdEndpoint;
				if (qStringParameters && qStringParameters != null) {
					file.url += qStringParameters;
				}
				else {
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non valida.');
					return false;
				}
			}
			else {
				Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non valida.');
				return false;
			}
			
			Utils.openModalConfirm(this, 'textTitle', 'htmlBody');
			
//			if (!window.confirm('Confermi la richiesta di generazione e download del file xls?')) {
//				return false;
//			}
			
			Utils.spinnerStart();
			file.fetch({
				success: function (entity, response, options) {
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					if (response.result && response.result.sessionId && response.result.sessionId != '') {
						// eventuale elaborazione della risposta
					}
					$('#downloadAnchorId').prop('href', engineRelativeRestServiceUrl + '/' + downloadEndpoint);
					$('#downloadAnchorId')[0].click();
					Utils.spinnerStop();
				},
				error: function (response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		sendJobCommandList: function (scope, event, listToSave, queryString) {
			this.sendJobCommandListInternal.call(scope, event, listToSave, queryString);
		},
		
		sendJobCommandListInternal: function (event, listToSave, queryString) {
			var self = this;
			
			listToSave.url = listToSave.url + queryString;
			
			Utils.spinnerStart();
			listToSave.save = function() {
				
				Backbone.sync('create', this, {
					
						wait: true,
						
						success: function(response, message) {
							if (!Utils.checkResponseError(this, response)) {
								return false;
							};
							var respObjs = response.result.jobs;
							var uncheckedJobs = $('input:checkbox:not(:checked)').parent().parent().parent().parent();
							uncheckedJobs.addClass('not-remove-job');
							_.each(respObjs, function(respJob) {
								var jobTrTmp = $('tr[id="' + respJob.jobId + '"]');
								if ($(jobTrTmp).find('input:checkbox:checked').length > 0) {
									$(jobTrTmp).attr('data-status', respJob.status);
									if ($(jobTrTmp).find('td[data-type="status-cell"]').length > 0) {
										$($(jobTrTmp).find('td[data-type="status-cell"]')[0]).attr('data-id', respJob.status);
									}
									if ($(jobTrTmp).find('img').length > 0) {
										$($(jobTrTmp).find('img')[0]).prop('src', 'img/' + respJob.status + '.png');
									}
									$(jobTrTmp).addClass('not-remove-job');
								}
								
							});
							$('.searchable tr').not('.not-remove-job').remove();
							Utils.spinnerStop();
						},
						error: function(response, error) {
							Utils.spinnerStop();
//							alert('ERRORE: chiamata ai servizi non andata a buon fine.');
							Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
						}
						
				});
				
			}
			listToSave.save();
		},
		
		sendLoadJobCommandList: function (scope, event, listToSave, queryString) {
			this.sendLoadJobCommandListInternal.call(scope, event, listToSave, queryString);
		},
		
		sendLoadJobCommandListInternal: function (event, listToSave, queryString) {
			var self = this;
			
			listToSave.url = listToSave.url + queryString;
			
			Utils.spinnerStart();
			listToSave.save = function() {
				
				Backbone.sync('create', this, {
					
						wait: true,
						
						success: function(response, message) {
							if (!Utils.checkResponseError(this, response)) {
								return false;
							};
							var respObjs = response.result.jobs;
							var uncheckedJobs = $('input:checkbox:not(:checked)').parent().parent().parent().parent();
							uncheckedJobs.addClass('not-remove-job');
							_.each(respObjs, function(respJob) {
								var jobTrTmp = $('tr[id="' + respJob.jobId + '"]');
								if ($(jobTrTmp).find('input:checkbox:checked').length > 0) {
									$(jobTrTmp).attr('data-status', respJob.status);
									// Nel caricamento c'è un solo stato...
									$(jobTrTmp).find('td[data-type="status-cell"]').attr('data-id', respJob.status);
									if (!respJob.status || respJob.status == 'ND') {
										$(jobTrTmp).find('img').prop('src', 'img/TRANSPARENT.gif');
									}
									else {
										$(jobTrTmp).find('img').prop('src', 'img/' + respJob.status + '.png');
									}
									$(jobTrTmp).addClass('not-remove-job');
								}
								
							});
							$('.searchable tr').not('.not-remove-job').remove();
							Utils.spinnerStop();
						},
						error: function(response, error) {
							Utils.spinnerStop();
//							alert('ERRORE: chiamata ai servizi non andata a buon fine.');
							Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
						}
						
				});
				
			}
			listToSave.save();
		},
		
		sendLoadAllJobCommandList: function (scope, event, listToSave, queryString) {
			this.sendLoadAllJobCommandListInternal.call(scope, event, listToSave, queryString);
		},
		
		sendLoadAllJobCommandListInternal: function (event, listToSave, queryString) {
			var self = this;
			
			listToSave.url = listToSave.url + queryString;
			
			Utils.spinnerStart();
			listToSave.save = function() {
				
				Backbone.sync('create', this, {
					
						wait: true,
						
						success: function(response, message) {
							if (!Utils.checkResponseError(this, response)) {
								return false;
							};
							var respObjs = response.result.jobs;
//							var uncheckedJobs = $('input:checkbox:not(:checked)').parent().parent().parent().parent();
//							uncheckedJobs.addClass('not-remove-job');
							_.each(respObjs, function(respJob) {
								var jobTrTmp = $('tr[id="' + respJob.jobId + '"]');
//								if ($(jobTrTmp).find('input:checkbox:checked').length > 0) {
									$(jobTrTmp).attr('data-status', respJob.status);
									$(jobTrTmp).find('td[data-type="status-cell"]').attr('data-id', respJob.status);
									
									if (!respJob.status || respJob.status == 'ND') {
										$(jobTrTmp).find('img').prop('src', 'img/TRANSPARENT.gif');
									}
									else {
										$(jobTrTmp).find('img').prop('src', 'img/' + respJob.status + '.png');
									}
									
									$(jobTrTmp).addClass('not-remove-job');
//								}
								
							});
							$('.searchable tr').not('.not-remove-job').remove();
							Utils.spinnerStop();
						},
						error: function(response, error) {
							Utils.spinnerStop();
//							alert('ERRORE: chiamata ai servizi non andata a buon fine.');
							Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
						}
						
				});
				
			}
			listToSave.save();
		},
		
		sendClusterLoadJobCommandList: function (scope, event, listToSave, queryString) {
			this.sendClusterLoadJobCommandListInternal.call(scope, event, listToSave, queryString);
		},
		
		sendClusterLoadJobCommandListInternal: function (event, listToSave, queryString) {
			var self = this;
			
			listToSave.url = listToSave.url + queryString;
			
			Utils.spinnerStart();
			listToSave.save = function() {
				
				Backbone.sync('create', this, {
					
						wait: true,
						
						success: function(response, message) {
							if (!Utils.checkResponseError(this, response)) {
								return false;
							};
							var respObjs = response.result.jobs;
							var uncheckedJobs = $('input:checkbox:not(:checked)').parent().parent().parent().parent();
							uncheckedJobs.addClass('not-remove-job');
							_.each(respObjs, function(respJob) {
								var jobTrTmp = $('tr[id="' + respJob.jobId + '"]');
								if ($(jobTrTmp).find('input:checkbox:checked').length > 0) {
									$(jobTrTmp).attr('data-status', respJob.status);
									if ($(jobTrTmp).find('td[data-type="status-cell"]').length > 0) {
										$($(jobTrTmp).find('td[data-type="status-cell"]')[0]).attr('data-id', respJob.status);
									}
									if ($(jobTrTmp).find('img').length > 0) {
										$($(jobTrTmp).find('img')[0]).prop('src', 'img/' + respJob.status + '.png');
									}
									$(jobTrTmp).addClass('not-remove-job');
								}
								
							});
							$('.searchable tr').not('.not-remove-job').remove();
							Utils.spinnerStop();
						},
						error: function(response, error) {
							Utils.spinnerStop();
//							alert('ERRORE: chiamata ai servizi non andata a buon fine.');
							Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
						}
						
				});
				
			}
			listToSave.save();
		},
		
		concatParametersList: function (scope, idParamName, nameListParameters, valueParamName, valueListParameters) {
			return this.concatParametersListInternal.call(scope, idParamName, nameListParameters, valueParamName, valueListParameters);
		},
		
		concatParametersListInternal: function (idParamName, nameListParameters, valueParamName, valueListParameters) {
			var queryString = '';
			var separator = '';
			if (nameListParameters && nameListParameters.length > 0) {
				for (var q=0; q<nameListParameters.length; q++) {
					queryString += (separator + idParamName + '=' + nameListParameters[q]);
					separator = '&';
				}
				for (var q=0; q<nameListParameters.length; q++) {
					queryString += (separator + valueParamName + '=' + valueListParameters[q]);
				}
			}
			return queryString;
		}
		
	});
	
	return CommandService;

});

