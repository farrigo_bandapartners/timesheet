
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/timetracker/loadingTemplate.htm',
	'bootstrap',
	'jquery.fileupload',
	'bootstrap.datepicker'
], function ($, _, Backbone, LoadingTemplate) {
	
	$.fn.datetimepicker.dates['it'] = {
   		days: ["Domenica", "Lunedi", "Martedi", "Mercoledi", "Giovedi", "Venerdi", "Sabato", "Domenica"],
   		daysShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom"],
   		daysMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa", "Do"],
   		months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
   		monthsShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
   		today: "Oggi",
   		suffix: [],
   		meridiem: [],
   		weekStart: 1,
   		format: "dd/mm/yyyy hh:ii:ss"
   	};
	
	var Utils = $.fn.extend({
		
		JOBS_UPLOADED: 'JOBS_FILE_UPLOADED',
		SESSION_EXPIRED_CODE: '440',
		
		spinnerStart: function() {
			var loadingTpl = _.template(LoadingTemplate);
			$('#my-modal-loading-spinner').html(loadingTpl);
			$('#my-modal-loading-spinner').show();
		},
		
		spinnerStop: function() {
			$('#my-modal-loading-spinner').hide();
		},
		
//		destroyView: function(view) {
//		    // unbind the view
//			view.undelegateEvents();
//			view.$el.removeData().unbind(); 
//		    // remove view from DOM
//			view.remove();  
//		    Backbone.View.prototype.remove.call(view);
//		},
		
//		unloadView: function(view) {
//		    // unbind the view
//			view.undelegateEvents();
//			view.$el.removeData().unbind(); 
//		    // remove view from DOM
//			view.remove();  
//		    Backbone.View.prototype.remove.call(view);
//		},
		
		getBreadcrum: function(scope, viewItemId) {
			return this.getBreadcrumInternal.call(scope, viewItemId);
		},
		
		getBreadcrumInternal: function(viewItemId) {
			var itemIndex = parseInt(viewItemId, 10);
			var breadcrum = '';
			var viewItem = this.menuItemList[itemIndex];
			var levelTmp = parseInt(viewItem.level, 10);
			var startLevel = 1;
			breadcrum = viewItem.label;
			if (levelTmp <= 1) {
				startLevel = 0;
			}
			// si prende il percorso dell'albero del menù dal livello n al livello startLevel + 1
			var itemTmp = null;
			while (levelTmp > startLevel) {
				itemIndex--;
				itemTmp = this.menuItemList[itemIndex];
				if (itemTmp.level < levelTmp) {
					breadcrum = itemTmp.label + ' &gt; ' + breadcrum;
					levelTmp--;
				}
			}
			return breadcrum;
		},
		
		getFlowStepName: function(scope, viewItemId) {
			return this.getFlowStepNameInternal.call(scope, viewItemId);
		},
		
		getFlowStepNameInternal: function(viewItemId) {
			var itemIndex = parseInt(viewItemId, 10);
			var migrationName = '';
			var viewItem = this.menuItemList[itemIndex];
			var levelTmp = parseInt(viewItem.level, 10);
			var itemTmp = null;
			while (levelTmp > 1) {
				itemIndex--;
				itemTmp = this.menuItemList[itemIndex];
				if (itemTmp.level == 2) {
					migrationName = itemTmp.label;
					break;
				}
			}
			return migrationName;
		},
		
		navigateLeftMenu: function(scope, event) {
			this.navigateLeftMenuInternal.call(scope, event);
		},
		
		navigateLeftMenuInternal: function(event) {
			var viewItemId = $(event.currentTarget).attr('data-id');
			var startItem = null;
			var parentState;
			var menuItem;
			var currElem;
			for (var j=0; j<this.menuItemList.length; j++) {
				menuItem = this.menuItemList[j];
				currElem = $('li[data-id="' + menuItem.id + '"]');
				if (menuItem.id == viewItemId) {
					startItem = menuItem;
					if (currElem.hasClass('item-closed')) {
						currElem.removeClass('item-closed');
						currElem.addClass('item-opened');
						parentState = 'opened';
					}
					else {
						currElem.removeClass('item-opened');
						currElem.addClass('item-closed');
						parentState = 'closed';
					}
				}
				else if (startItem != null) { // All'iterazione successiva
					// se sono i discendenti diretti dell'elemento cliccato ...
					if (menuItem.level == parseInt(startItem.level, 10) + 1) {
						if (parentState == 'opened') {
							currElem.removeClass('item-hidden');
							currElem.addClass('item-visible');
							currElem.show();
						}
						else {
							currElem.removeClass('item-visible');
							currElem.addClass('item-hidden');
							currElem.hide();
						}
					}
					// se sono discendenti di livello più basso nell'albero ...
					else if (menuItem.level > parseInt(startItem.level, 10) + 1) {
						if (parentState == 'opened') {
							if (currElem.hasClass('item-visible')) {
								currElem.show();
							}
							else {
								currElem.hide();
							}
						}
						else {
							currElem.hide();
						}
					}
					// se è l'elemento di pari livello successivo a quello cliccato nell'albero ...
					else if (menuItem.level == startItem.level) {
						break;
					}
				}
			}
		},
		
		tableSearch: function (scope, event, tableId) {
			this.tableSearchInternal.call(scope, event, tableId);
		},
		
		tableSearchInternal: function (event, tableId) {
			var element = $(event.currentTarget);
			if (tableId) {
				// la RegExp seguente è case insensitive ('i')
				var rex = new RegExp(element.val(), 'i');
	            $('table[data-id="' + tableId + '"] tbody.searchable tr').hide();
	            $('table[data-id="' + tableId + '"] tbody.searchable tr').filter(function () {
	            	// nella riga seguente, $(this) è l'elemento (uno degli elementi)
	            	// individuato dal selettore 'tbody.searchable tr', su cui è applicata
	            	// la funzione .filter()
	                return rex.test($(this).text());
	            }).show();
			}
			else {
				// la RegExp seguente è case insensitive ('i')
	            var rex = new RegExp(element.val(), 'i');
	            $('tbody.searchable tr').hide();
	            $('tbody.searchable tr').filter(function () {
	            	// nella riga seguente, $(this) è l'elemento (uno degli elementi)
	            	// individuato dal selettore 'tbody.searchable tr', su cui è applicata
	            	// la funzione .filter()
	                return rex.test($(this).text());
	            }).show();
			}
		},
		
//		tableTdSearchCombo: function (scope, event, activeFilterArray) {
//			this.tableTdSearchComboInternal.call(scope, event, activeFilterArray);
//		},
//		
//		tableTdSearchComboInternal: function (event, activeFilterArray) {
//			// rivisualizzo tutte le righe
//			$("tbody.searchable tr").show();
//			// riapplico i filtri correnti
//			for (var y=0; y < activeFilterArray.length; y++) {
//				$("tbody.searchable tr").find("td[data-column-index='" + activeFilterArray[y].columnIndex + "']:visible").filter(function() {
//					var rex = new RegExp(activeFilterArray[y].text, 'i');
//					if (activeFilterArray[y].type == 'filter-text-type') {
//						return !rex.test($(this).text());
//					}
//					else if (activeFilterArray[y].type == 'filter-select-type') {
//						return !rex.test($(this).attr('data-id'));
//					}
//				}).parent().hide();
//			}
//		},
					
		tableTdSearch: function (scope, event, activeFilterArray) {
			this.tableTdSearchInternal.call(scope, event, activeFilterArray);
		},
		
		tableTdSearchInternal: function (event, activeFilterArray) {
			// rivisualizzo tutte le righe
			$("tbody.searchable tr").show();
			// riapplico i filtri correnti
			for (var y=0; y < activeFilterArray.length; y++) {
				$("tbody.searchable tr").find("td[data-column-index='" + activeFilterArray[y].columnIndex + "']:visible").filter(function() {
					var rex = new RegExp(activeFilterArray[y].text, 'i');
					
					return !rex.test($(this).text());
					
//					if (activeFilterArray[y].type == 'filter-text-type') {
//						return !rex.test($(this).text());
//					}
//					else if (activeFilterArray[y].type == 'filter-select-type') {
//						return !rex.test($(this).attr('data-id'));
//					}
					
				}).parent().hide();
			}
		},
		
		setUploader: function (scope, restEndPoint, elementId, migrationTypeId, filterIdList, filterValueList) {
			this.setUploaderInternal.call(scope, this, restEndPoint, elementId, migrationTypeId, filterIdList, filterValueList);
		},
		
		// Aggiunge la funzionalita' di upload di file xls (JQuery File Upload Plugin)
		// 'engineRelativeRestServiceUrl' è una variabile globale definita in 'home.jsp'.
		// Il plugin di JQuery necessita di un url relativo.
		setUploaderInternal: function (utilsScope, restEndPoint, elementId, migrationTypeId, filterIdList, filterValueList) {
			$('#btn-add-upload-file-id').off('click');
			utilsScope.resetFileUpload();
			$('#' + elementId).fileupload({
				url: engineRelativeRestServiceUrl + '/' + restEndPoint,
		        dataType: 'json',
		        add: function (e, data) {
		        	var uploadErrors = [];
		            var acceptFileTypes = /(\.)(xls||xlsx)$/i;
		            var fileName = data.originalFiles[0].name;
		            if (!fileName || !acceptFileTypes.test(fileName)) {
		                uploadErrors.push('tipo di file non consentito per l\'upload;');
		            }
		            if (data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
		                uploadErrors.push('dimensione del file in upload non consentita;');
		            }
		            if (uploadErrors.length > 0) {
		            	utilsScope.openModalWarningInternal(utilsScope, 'Attenzione:', uploadErrors.join("<br/>"));
//		                alert(uploadErrors.join("\n"));
		                return false;
		            } 
//		            else {
//		                data.submit();
//		            }
		        	$('#btn-cancel-upload-file-id').show();
		        	$('#btn-start-upload-file-id').show();
		        	$('#btn-cancel-upload-file-id').on('click', function (event) {
		        		data.files.length = 0;
		        		utilsScope.resetFileUpload();
		        	});
		        	$('#file-to-upload-id').html(data.files[0].name + ' &nbsp;&nbsp;&nbsp;');
		        	$('#btn-start-upload-file-id').click(function () {
	//                    data.context = $('<p/>').text('Uploading...').replaceAll($(this));
		        		utilsScope.spinnerStart();
	                    data.submit();
	                });
	//	            data.context = $('#submit-file-button').show()
	////	                .appendTo($('#fileupload'))
	//	                .click(function () {
	//	                    data.context = $('<p/>').text('Uploading...').replaceAll($(this));
	//	                    data.submit();
	//	                });
		        	$('#btn-add-upload-file-id').prop("disabled", true);
		        },
		        
//		        submit: function (e, data) {
//		        	this.formData = {params: 'exx2k'};
//		        },
		        
		        formData: function(data) {
		        	var arrayTmp = [];
		        	if (filterIdList && filterIdList.length > 0) {
		        		arrayTmp = utilsScope.filterToJsonList(filterIdList, filterValueList);
		        	}
		        	if (migrationTypeId && migrationTypeId != '') {
		        		arrayTmp.push({name: 'migrationTypeId', value: migrationTypeId});
		        	}
		        	return arrayTmp;
		        },
		        
		        fail: function (e, data) {
		        	utilsScope.spinnerStop();
//		        	unsubscribeForUpdates();
		        	utilsScope.openModalErrorInternal(utilsScope, 'ERRORE:',
		        								'Errore nel caricamento del file ' + data.files[0].name + '');
//		        	alert('Errore nel caricamento del file ' + data.files[0].name + '');
		        	data.files.length = 0;
		        	utilsScope.resetFileUpload();
		        },
		        
		        done: function (e, data) {
		        	// Controllo su eventuali errori applicativi
					if (!utilsScope.checkResponseError(this, data.result)) {
						return false;
					};
		        	utilsScope.spinnerStop();
//		        	alert('File ' + data.files[0].name + ' caricato correttamente');
		        	utilsScope.resetFileUpload();
		        	$('#file-to-upload-id').html('File <b>' + data.files[0].name + '</b> caricato correttamente &nbsp;&nbsp;&nbsp;');
		        	data.files.length = 0;
//		        	data.context.text('Upload finished.');

		        	// Evento custom definito in Utils
		        	$('body').trigger(utilsScope.JOBS_UPLOADED);
		        	
		        }
		    });
			$('#btn-add-upload-file-id').on('click', function(event) {$('#fileupload').click();});
		},
		
		resetFileUpload: function () {
    		$('#btn-cancel-upload-file-id').off('click');
    		$('#btn-start-upload-file-id').off('click');
    		$('#btn-cancel-upload-file-id').hide();
    		$('#btn-start-upload-file-id').hide();
    		$('#file-to-upload-id').html('');
    		$('#btn-add-upload-file-id').prop("disabled", false);
        },
        
        checkResponseError: function (scope, response) {
        	return this.checkResponseErrorInternal.call(scope, this, response);
        },
        
        checkResponseErrorInternal: function(utilsScope, response) {
        	// TODO: to be reengineered.
        	// per ora questa if evita desincronizzazioni tra back end
        	// e front end... da eliminare, a regime...
        	if (response && response.result && response.result.error) {
        		console.log('ERRORE -:' + response.result.message);
        		utilsScope.spinnerStop();
        		var exitFlag = false;
        		if (response.result.code == utilsScope.SESSION_EXPIRED_CODE) {
//        			unsubscribeForUpdates();
        			exitFlag = true;
        		}
				utilsScope.openModalErrorInternal(utilsScope, 'ERRORE:', response.result.message, exitFlag);
				return false;
			}
        	else if (response && response.error) {
        		console.log('ERRORE -:' + response.message);
        		utilsScope.spinnerStop();
        		var exitFlag = false;
        		if (response.code == utilsScope.SESSION_EXPIRED_CODE) {
//        			unsubscribeForUpdates();
        			exitFlag = true;
        		}
				utilsScope.openModalErrorInternal(utilsScope, 'ERRORE:', response.message, exitFlag);
				return false;
			}
        	else {
        		return true;
        	}
        },
        
        openModalPopup: function(scope, textTitle, htmlBody) {
        	return this.openModalPopupInternal.call(scope, this, textTitle, htmlBody);
        },
        
        openModalPopupInternal: function(utilsScope, textTitle, htmlBody) {
        	$('#fmwk-div-modal-popup').modal('show');
        	$('#fmwk-popup-modal-title').text(textTitle);
        	$('#fmwk-popup-modal-body').html(htmlBody);
        	$('#fmwk-content-modal-popup').removeClass('fmwk-div-hide');
        	$('#fmwk-close-modal-popup').click(function (event) {
        		$('#fmwk-content-modal-popup').addClass('fmwk-div-hide');
//        		console.log('---- close popup ----');
        	});
        },
        
        forceCloseModal: function(scope) {
        	return this.forceCloseModalInternal.call(scope, this);
        },
        
        forceCloseModalInternal: function(utilsScope) {
        	$('#fmwk-content-modal-popup').addClass('fmwk-div-hide');
        	$('#fmwk-div-modal-popup').click();
        },
        
        openModalError: function(scope, textTitle, htmlBody) {
        	return this.openModalErrorInternal.call(scope, this, textTitle, htmlBody);
        },
        
        openModalErrorInternal: function(utilsScope, textTitle, htmlBody, exitFlag) {
//        	unsubscribeForUpdates();
        	$('#fmwk-div-modal-error').modal('show');
        	$('#fmwk-modal-error-title').text(textTitle);
        	$('#fmwk-modal-error-body').html(htmlBody);
        	$('#fmwk-content-modal-error').removeClass('fmwk-div-hide');
        	$('#fmwk-close-modal-error').click(function (event) {
        		$('#fmwk-content-modal-error').addClass('fmwk-div-hide');
        		if (exitFlag) {
        			// fmwkGuiLoginPage: variabile globale definita in home.jsp
        			document.location.href = fmwkGuiLoginPage;
        		}
//        		console.log('---- close error popup ----');
        	});
        },
        
        openModalWarning: function(scope, textTitle, htmlBody) {
        	return this.openModalWarningInternal.call(scope, this, textTitle, htmlBody);
        },
        
        openModalWarningInternal: function(utilsScope, textTitle, htmlBody) {
        	$('#fmwk-div-modal-error').modal('show');
        	$('#fmwk-modal-error-title').text(textTitle);
        	$('#fmwk-modal-error-body').html(htmlBody);
        	$('#fmwk-content-modal-error').removeClass('fmwk-div-hide');
        	$('#fmwk-close-modal-error').click(function (event) {
        		$('#fmwk-content-modal-error').addClass('fmwk-div-hide');
//        		console.log('---- close warning popup ----');
        	});
        },
        
        openModalConfirm: function(scope, textTitle, htmlBody, callBackYes, callBackNo) {
        	return this.openModalConfirmInternal.call(this, scope, this, textTitle, htmlBody, callBackYes, callBackNo);
        },
        
        openModalConfirmInternal: function(scope, utilsScope, textTitle, htmlBody, callBackYes, callBackNo) {
        	$('#btn-modal-confirm-y-id').off('click');
        	$('#btn-modal-confirm-n-id').off('click');
        	$('#fmwk-div-modal-confirm').modal('show');
        	$('#fmwk-modal-confirm-title').text(textTitle);
        	$('#fmwk-modal-confirm-body').html(htmlBody);
        	$('#fmwk-content-modal-confirm').removeClass('fmwk-div-hide');
        	$('#btn-modal-confirm-y-id').on('click', function(){callBackYes.call(scope)});
        	$('#btn-modal-confirm-n-id').on('click', function(){callBackNo.call(scope)});
        },
        
        filterToJsonList: function(filterIdList, filterValueList) {
        	var retVal = [];
        	if (filterIdList && filterIdList.length > 0) {
        		for (var i=0; i < filterIdList.length; i++) {
        			retVal.push({name: 'filterId', value: filterIdList[i]});
        			retVal.push({name: 'filterValue', value: filterValueList[i]});
        		}
        	}
        	return retVal;
        },
        
        setDatePicker: function(scope, elementId) {
        	return this.setDatePickerInternal.call(this, scope, this, elementId);
        },
        
        setDatePickerInternal: function(scope, utilsScope, elementId) {
        	$('#' + elementId).datetimepicker({
				language: 'it',
				format: 'dd/mm/yyyy',
			    minView: 2,
			    autoclose: true 
        	});
        },
        
        setTimePicker: function(scope, elementId) {
        	return this.setTimePickerInternal.call(this, scope, this, elementId);
        },
        
        setTimePickerInternal: function(scope, utilsScope, elementId) {
        	$('#' + elementId).datetimepicker({
			    language: 'it',
		    	format: 'HH:00',
			    startView: 1,
			    minView: 1,
			    pickDate: false,
			    autoclose: true,
			    steps: 60
//			    ,
//			    startDate: utilsScope.getPresentDateTime()
			})
			.on("show", function() {
				$(".table-condensed .prev").css('visibility', 'hidden');
				$(".table-condensed .switch").text("Ora: ");
				$(".table-condensed .next").css('visibility', 'hidden');
			});
        	$('#' + elementId).data('datetimepicker').setDate(utilsScope.getDefaultDateTime());
        },
        
        getDefaultDateTime: function() {
        	var d = new Date();
        	var month = d.getMonth();
        	var day = d.getDate();
        	var year = d.getFullYear();
        	return new Date(year, month, day, '09', '00');
        }
		
	});
	
	return Utils;

});

