
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var Order = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/orders',
		
		defaults: {
			id: null
		}
		
	});
	
	return Order;
	
});