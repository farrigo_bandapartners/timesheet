
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var Resource = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/resources',
		
		defaults: {
			id: null
		}
		
	});
	
	return Resource;
	
});