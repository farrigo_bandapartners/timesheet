
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var Activity = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/activities',
		
		defaults: {
			id: null
		}
		
	});
	
	return Activity;
	
});