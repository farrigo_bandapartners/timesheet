
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var MenuItem = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/menuItems',
		
		defaults: {
			id: null,
			label: '',
			value: ''
		}
		
	});
	
	return MenuItem;
	
});