define([
	'jquery',
	'underscore'
], function ($, _) {
	
	var TextRowSearchFilter = function(index, type, text, active) {
		
		this.prototype = {
			// Default value
			columnIndex: -1,
			type: '',
			text: '',
			active: false
		}
		
		// Attenzione: per gli interi, il test if (index) risulta falso se index == 0
		if (index >= 0) {
			this.columnIndex = index;
		}
		if (type) {
			this.type = type;
		}
		if (text) {
			this.text = text;
		}
		if (active) {
			this.active = active;
		}
		
	};
	
	return TextRowSearchFilter;
	
});
