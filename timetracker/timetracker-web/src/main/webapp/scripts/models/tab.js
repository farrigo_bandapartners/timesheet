
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var Tab = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/tabs',
		
		defaults: {
			id: '',
			index: '0',
			active: false,
			label: ''
		}
		
	});
	
	return Tab;
	
});