
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var Allocation = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/allocations',
		
		defaults: {
			id: null
		}
		
	});
	
	return Allocation;
	
});