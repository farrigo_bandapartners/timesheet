
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var File = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/files',
		
		defaults: {
			id: null,
			sessionId: '',
			fileName: '',
			migrationTypeId: '',
			clusterId: '',
			tabId: ''
		}
		
	});
	
	return File;
	
});