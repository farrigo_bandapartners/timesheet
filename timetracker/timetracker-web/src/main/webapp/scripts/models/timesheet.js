

define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var Timesheet = Backbone.Model.extend({

//		urlRoot: engineRestServiceUrl + '/timesheets',
		urlRoot: engineRestServiceUrl + '/periods',
		
		defaults: {
			id: null,
			label: '',
			value: ''
		}
		
	});
	
	return Timesheet;
	
});