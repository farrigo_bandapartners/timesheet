
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	// Classe utilizzata per l'invio della richiesta d'esecuzione 
	// di un comando su di un cluster o su una lista di job.
	var JobCommand = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/jobCommands',
		
		defaults: {
			id: null,
			jobId: '',
			status: ''
			// Eventuali forzature di stato
		}
		
	});
	
	return JobCommand;
	
});
