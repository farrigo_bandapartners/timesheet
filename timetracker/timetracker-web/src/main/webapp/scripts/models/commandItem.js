
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	// Classe utilizzata per reperire la lista di comandi
	// per una determinata tab all'interno di una migrazione.
	var CommandItem = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/commandItems',
		
		defaults: {
			id: null,
			description: '',
			migrationTypeId: '',
			clusterId: '',
			tabId: '',
			jobId: ''
		}
		
	});
	
	return CommandItem;
	
});