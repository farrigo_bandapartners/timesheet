
define([
	'jquery',
	'underscore',
	'backbone'
], function ($, _, Backbone) {
	
	var ActivityHeader = Backbone.Model.extend({

		urlRoot: engineRestServiceUrl + '/activityHeaders',
		
		defaults: {
			id: null
		}
		
	});
	
	return ActivityHeader;
	
});