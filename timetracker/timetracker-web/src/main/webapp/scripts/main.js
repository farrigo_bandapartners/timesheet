
require.config({
	
	paths: {
		'jquery'					:	'../js/jquery/jquery-2.2.3',
		'underscore'				: 	'../js/underscore/underscore-1.8.3',
		'backbone'					: 	'../js/backbone/backbone-1.3.3',
		'bootstrap'					: 	'../js/bootstrap/bootstrap-3.3.6',
		'text'						: 	'../js/require/require-text-2.0.15',
		'jquery.ui.widget'			:	'../js/jquery-file-upload/js/vendor/jquery.ui.widget',
		'jquery.iframe-transport'	:	'../js/jquery-file-upload/js/jquery.iframe-transport',
		'jquery.fileupload'			:	'../js/jquery-file-upload/js/jquery.fileupload',
		'bootstrap.datepicker'		:	'../js/bootstrap-datetimepicker/bootstrap-datetimepicker'
	}

});

require([
	'app'
], 
function(App) {
	App.initialize();
});
