
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/allocation',
	'../../models/textRowSearchFilter',
	'../../views/timetracker/updateAllocationView',
	'../../utils',
	'text!../../templates/timetracker/searchAllocation.htm'
], function ($, _, Backbone, Allocation, TextRowSearchFilter, UpdateAllocationView, Utils, SearchAllocationTpl) {
	
	var SearchAllocationView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
			'keyup .filter-text-type' : 'applyFilter',
			
			'click tr.allocation-search-elem' : function (event) {
				this.loadAllocation(event);
			}
			
		},
		
		breadcrum : '',
		allocation : null,
		updateAllocationView : null,
		
		initialize: function() {
			this.allocation = new Allocation();
			this.updateAllocationView = new UpdateAllocationView();
		},
		
		render: function (sideMenuId, breadcrum) {
			this.breadcrum = breadcrum;
			var self = this;
			var tmpl = _.template(SearchAllocationTpl);
			this.allocation.fetch({
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					self.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									title		:	'CONTRATTI / ALLOCAZIONI',
									rowList		: 	response
//									rowList		: 	response.result.dataList
								})
					);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		loadAllocation: function(event) {
			var allocationId = $(event.currentTarget).attr('id');
			this.updateAllocationView.render(this.breadcrum, allocationId);
		},
		
		applyFilter: function (event) {
			var activeFilterArray = [];
			var textSearchFields = $(".filter-text-type");
			for (var y=0; y < textSearchFields.length; y++) {
				if ($(textSearchFields[y]).val() != '') {
					var dataColTmp = parseInt($(textSearchFields[y]).attr('data-column-index'), 10);
					var filter = new TextRowSearchFilter(dataColTmp, 'filter-text-type', $(textSearchFields[y]).val(), true);
					activeFilterArray.push(filter);
				}
			}
			Utils.tableTdSearch(this, event, activeFilterArray);
		}
		
	});
	
	return SearchAllocationView;
	
});
