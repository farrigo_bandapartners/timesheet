
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/order',
	'../../views/timetracker/updateOrderView',
	'../../utils',
	'text!../../templates/timetracker/searchOrder.htm'
], function ($, _, Backbone, Order, UpdateOrderView, Utils, SearchOrdTpl) {
	
	var SearchOrderView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
			'keyup #generic-text-search' : function (event) {
				Utils.tableSearch(this, event);
			},
			
			'click tr.order-search-elem' : function (event) {
				this.loadOrder(event);
			}
			
		},
		
		breadcrum : '',
		order : null,
		updateOrderView : null,
		
		initialize: function() {
			this.order = new Order();
			this.updateOrderView = new UpdateOrderView();
		},
		
		render: function (sideMenuId, breadcrum) {
			this.breadcrum = breadcrum;
			var self = this;
			var tmpl = _.template(SearchOrdTpl);
			this.order.fetch({
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					self.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									title		:	'LISTA ORDINI',
									rowList		: 	response
//									rowList		: 	response.result.dataList
								})
					);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		loadOrder: function(event) {
			var orderId = $(event.currentTarget).attr('id');
			this.updateOrderView.render(this.breadcrum, orderId);
		}
		
	});
	
	return SearchOrderView;
	
});
