
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/resource',
	'../../utils',
	'text!../../templates/timetracker/search.htm'
], function ($, _, Backbone, Resource, Utils, SearchTpl) {
	
	var SearchView = Backbone.View.extend({
		
		el: $('#fmwk-div-modal-popup'),
		
		events: {
			
			'keyup #generic-text-search' : function (event) {
				Utils.tableSearch(this, event, 'generic-search-id');
			},
			
			'click tr.popup-search-elem' : 'pickElement'
				
		},
		
		parentView: null,
		breadcrum : '',
		resource : null,
		
		initialize: function() {
			this.resource = new Resource();
		},
		
		render: function(title, dataList) {
			var tmpl = _.template(SearchTpl);
			
			var popupHTML = '';
			popupHTML += '<table class="table table-striped table-hover external-bordered">';
			popupHTML += '<tbody>';
			var searchDataList = dataList;
			_.each(searchDataList, function(rowTmp) {
				popupHTML += '<tr class="resource" data-id="' + rowTmp.id + '">';
				popupHTML += '<td style="">&nbsp;';
				popupHTML += rowTmp.label;
				popupHTML += '</td>';
				popupHTML += '<td>&nbsp;</td>';
				popupHTML += '<td> ';
				popupHTML += rowTmp.value;
				popupHTML += '&nbsp;</td>';
				popupHTML += '</tr>';
			});
			popupHTML += '</tbody>';
			popupHTML += '</table>';
//			console.log(popupHTML);
			
			var searchHTML = tmpl({
									rowList		: 	searchDataList
							});
			
			Utils.openModalPopup(this, title, searchHTML);
		},
		
		pickElement: function(event) {
			this.parentView.selectElem(event);
		}
		
	});
	
	return SearchView;
	
});
