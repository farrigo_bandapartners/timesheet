
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/order',
	'../../models/resource',
	'../../models/allocation',
	'../../utils',
	'text!../../templates/timetracker/updateAllocation.htm'
], function ($, _, Backbone, Order, Resource, Allocation, Utils, UpdateAllocationTpl) {
	
	var SchedulingView = Backbone.View.extend({
		
		el: $('#container'),
		
		RESOURCE_TYPE : 'resource',
		ORDER_TYPE : 'order',
		
		events: {
			
			'click #btn-upd-schedule-id' : 'updSchedule'
			
		},
		
		searchType : '',
		breadcrum : '',
		order : null,
		resource : null,
		allocation: null,
		orderId : '',
		resourceId : '',
		allocationId: '',
		orderDesc : '',
		resourceDesc : '',
		allocationDesc : '',
		
		initialize: function() {
			this.order = new Order();
			this.resource = new Resource();
			this.allocation = new Allocation();
		},
		
		reset: function(allocationId) {
			this.allocation = new Allocation({id: allocationId});
		},
		
		render: function (breadcrum, allocationId) {
			this.breadcrum = breadcrum;
			this.allocationId = allocationId;
			this.orderId = '';
			this.resourceId = '';
			this.reset(allocationId);
			var self = this;
			
			this.allocation.fetch({
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					self.renderAllocation(response);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
 		
		renderAllocation: function(allocation) {
			var self = this;
			var tmpl = _.template(UpdateAllocationTpl);
			
			this.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									allocation	:	allocation
								})
			);
			
			Utils.setDatePicker(this, 'id-datetimepicker-from-date');
			Utils.setDatePicker(this, 'id-datetimepicker-to-date');
		},
		
		updSchedule: function(event) {
			if (this.isValidInputData()) {
				return true;
			}
			else {
				return false;
			}
		},
		
		isValidInputData: function() {
			
			var dateFromTmp = $('#functLogDateFromId').val();
			var dateToTmp = $('#functLogDateToId').val();
			
			if (!dateFromTmp || dateFromTmp.length == 0 || !dateToTmp || dateToTmp.length == 0) {
				Utils.openModalError(this, 'Dati non validi:', 'I parametri Data inizio e Data fine devono essere valorizzati.');
				return false;
			}
			else {
				Utils.openModalPopup(this, 'Allocazione salvata con successo:', 'Le modifiche alle date sono state apportate correttamente.');
				return true;
			}
		}
		
	});
	
	return SchedulingView;
	
});
