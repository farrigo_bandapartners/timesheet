
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/jobCommand',
	'../../models/tab',
	'./timesheetTabView',
	'../../utils',
	'../../commandService',
	'text!../../templates/timetracker/content.htm'
], function ($, _, Backbone, JobCommand, 
						Tab, TimesheetTabView, Utils, CommandService, Template) {
	
	var ContentView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			'click .fmwkTabHeader' : 'hideShowTabs',
			'click .fmwk-command' : 'sendCommand',
			'click #btn-export-xls-file-id' : 'confirmExportXls'
		},
		
		sideMenuId: '',
		timesheetId: '',
		timesheetLabel: '',
		tabViewArray: null,
		tab: null,
		tabListResponse: null,
		tabActive: 0,
		
		initialize: function() {
			this.tab = new Tab();
			this.tabListResponse = [];
			this.reset();
		},
		
		reset: function() {
			if (this.tabViewArray != null) {
				this.tabViewArray.length = 0;
				this.tabViewArray = [];
			}
			else {
				this.tabViewArray = [];
			}
			this.tabActive = 0; // tab di default
			this.sideMenuId = '';
		},
					
		render: function (sideMenuId, timesheetLabel, timesheetId) {
			this.reset();
			var self = this;
			
			this.sideMenuId = sideMenuId;
			this.timesheetId = timesheetId;
			this.timesheetLabel = timesheetLabel;
			
			Utils.spinnerStart();
			
			this.tab.fetch({
//				data: {	
//						sideMenuId: sideMenuId
//					  },
				success: function (collection, response, options) {
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					var respContentLabel = timesheetLabel;
					
					if (response.result.flowName && response.result.flowName != '') {
						respContentLabel = response.result.flowName;
					}
					
					if (response.result.clusterId && response.result.clusterId != '') {
						self.clusterId = response.result.clusterId;
					}
					
					var respTabs = response.result.tabList;
					
					self.tabListResponse = respTabs;
					_.each(respTabs, function(tab) {
						var tabView = new TimesheetTabView();
						tabView.sideMenuId = self.sideMenuId;
						tabView.tabId = tab.id;
						tabView.index = tab.index;
						self.tabViewArray.push(tabView);
						if (tab.active === true) {
							self.tabActive = tab.index;
						}
					});
					var tmpl = _.template(Template);
					self.$el.html(tmpl({	
											tabList: respTabs, 
											tabActive: self.tabActive,
											timesheetLabel: self.timesheetLabel
										}));
					self.renderTabs();
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		renderTabs: function() {
			var self = this;
			var viewItem = null;
			_.each(this.tabViewArray, function(viewItem) {
				if (viewItem.index == self.tabActive) {
					viewItem.$el = $('#' + viewItem.tabId); // viewItem è di tipo TimesheetTabView
					viewItem.sideMenuId = self.sideMenuId;
					viewItem.render(viewItem.tabId);
				}
			});
		},
		
		/**
		 * Gestione dei tab dinamici
		 * In questa prima versione, ad ogni cambio di tab, 
		 * il suo contenuto verrà completamente ricaricato.
		 */
		hideShowTabs: function(event) {
			var self = this;
			var indexEvent = $(event.currentTarget).attr('data-id');
			$('.fmwkTabHeader').parent().removeClass('in active');
			$(event.currentTarget).parent().addClass('in active');
			$('.fmwkTabContent').removeClass('in active');
			$('div[data-id="' + $(event.currentTarget).attr('data-id') + '"].fmwkTabContent').addClass('in active');
			
			// Attenzione!! La cancellazione della vecchia tab va fatta prima del render della nuova!!
			var oldActive = this.tabActive;
			var oldTabId = this.tabViewArray[oldActive].tabId;
			$('#' + oldTabId).html('');
			
			this.tabActive = indexEvent;
			var tabId = '';
			_.each(this.tabViewArray, function(viewItem) {
				if (viewItem.index == self.tabActive) {
//					viewItem.$el = $('#' + viewItem.tabId);
//					viewItem.sideMenuId = self.sideMenuId;
//					viewItem.clusterId = self.clusterId;
//					viewItem.render(self.filterIdList, self.filterValueList);
					
					viewItem.$el = $('#' + viewItem.tabId); // viewItem è di tipo TimesheetTabView
					viewItem.sideMenuId = self.sideMenuId;
					viewItem.render(viewItem.tabId);
					
				}
			});
		},
		
		sendCommand: function (event) {
			console.log('sending command...');
		},
		
		confirmExportXls: function() {
			Utils.openModalConfirm(this, 'Conferma Operazione:', 'Esportare i dati in formato xls?', this.exportXlsFileYes, this.exportXlsFileNo);
		},
		
		exportXlsFileYes: function () {
			var qsParameters = 	'?sideMenuId=' + this.sideMenuId +
								'&clusterId=' + this.clusterId +
								'&tabId=' + this.tabListResponse[this.tabActive].id;
			
			CommandService.sendGetXlsCommand(this, 'exportXls', 'files', qsParameters);
		},
		
		exportXlsFileNo: function (event) {
			return false;
		}
		
	});
	
	return ContentView;
	
});

