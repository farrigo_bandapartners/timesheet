
define([
	'jquery',
	'underscore',
	'backbone',
	'../../utils',
	'../../commandService',
	'../../models/commandItem',
	'../../models/activity',
	'../../models/activityHeader',
	'text!../../templates/timetracker/timesheetTab.htm'
], function ($, _, Backbone, Utils, CommandService, CommandItem, Activity, ActivityHeader, Template) {
	
	var MigrationTabView = Backbone.View.extend({
		
		el: null,
		
		events: {
			// il binding degli eventi su questa pagina non si riesce a fare
			// perchè il suo contenuto (il suo DOM) viene cancellato quando si 
			// visualizza una nuova tab;
			// va effettuato sulla pagina che la contiene, la 'contentView.js'
		},
		
		tabId: null,
		headLabels: null, 
		tabRows: null,
		activity: null,
		activityHeader: null,
		activityList: null,
		commandItem: null,
		commandItemList: null,
		
		initialize: function() {
			this.reset();
		},
		
		reset: function(tabId) {
			this.commandItem = new CommandItem({id: tabId});
			this.activity = new Activity({id: tabId});
			this.activityHeader = new ActivityHeader({id: tabId});
		},
						
		render: function (tabId) {
			this.reset(tabId);
			var self = this;
			Utils.spinnerStart();
			this.commandItem.fetch({
//				data: $.param({	
//								tabId: self.tabId
//							}),
				success: function (collection, response, options) {
					
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					var respObjs = response.result.commandTabItems;
					self.commandItemList = respObjs;
					self.loadHeadLabelList();
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		loadHeadLabelList: function() {
			var self = this;
			this.activityHeader.fetch({
//				data: $.param({	
//								tabId: self.tabId
//							}),
				success: function (collection, response, options) {
					
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					var respObjs = response.result.headers;
					self.headLabels = respObjs;
					self.loadActivityList();
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		loadActivityList: function() {
			var self = this;
			this.activity.fetch({
				
//				data: {	
//						tabId: self.tabId
//				},
						
				success: function (collection, response, options) {
					
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					var respObjs = response.result.jobs;
					self.tabRows = respObjs;
					self.renderAll();
					Utils.spinnerStop();
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		renderAll: function() {
			var tmpl = _.template(Template);
			this.$el.html(tmpl({
									headLabels		: this.headLabels,
									tabRows			: this.tabRows,
									commandList 	: this.commandItemList,
									tabId			: this.tabId
								})
			);
		}
	
	});
	
	return MigrationTabView;
});
