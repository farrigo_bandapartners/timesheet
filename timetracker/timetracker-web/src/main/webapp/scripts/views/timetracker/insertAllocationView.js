
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/order',
	'../../models/resource',
	'../../views/timetracker/searchView',
	'../../utils',
	'text!../../templates/timetracker/search.htm',
	'text!../../templates/timetracker/insertAllocation.htm'
], function ($, _, Backbone, Order, Resource, SearchView, Utils, SearchTpl, InsertAllocationTpl) {
	
	var SchedulingView = Backbone.View.extend({
		
		el: $('#container'),
		
		RESOURCE_TYPE : 'resource',
		ORDER_TYPE : 'order',
		
		events: {
			
			'click #btn-ins-order-id' : 'showModalOrders',
			
			'click #btn-ins-resource-id' : 'showModalResources',
			
			'click #btn-ins-schedule-id' : 'addSchedule'
			
		},
		
		searchView: null,
		searchType : '',
		breadcrum : '',
		order : null,
		resource : null,
		orderId : '',
		resourceId : '',
		orderDesc : '',
		resourceDesc : '',
		
		initialize: function() {
			this.order = new Order();
			this.resource = new Resource();
		},
		
		render: function (sideMenuId, breadcrum) {
			var self = this;
			this.breadcrum = breadcrum;
			
			this.orderId = '';
			this.resourceId = '';
			
			this.renderSchedule();
		},
 		
		renderSchedule: function() {
			var self = this;
			var tmpl = _.template(InsertAllocationTpl);
			
			this.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									columnCount	:	1
								})
			);
			
			Utils.setDatePicker(this, 'id-datetimepicker-from-date');
			Utils.setDatePicker(this, 'id-datetimepicker-to-date');
		},
		
		showModalOrders: function(event) {
			var self = this;
			self.searchType = self.ORDER_TYPE;
			this.order.fetch({
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					self.searchView.render('Attività aperte:', response);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
			
		},
		
		showModalResources: function(event) {
			var self = this;
			self.searchType = self.RESOURCE_TYPE;
			this.resource.fetch({
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					self.searchView.render('Risorse disponibili:', response);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
			
		},
		
		selectElem: function(event) {
			if (this.searchType === this.ORDER_TYPE) {
				this.selectOrder(event);
			}
			else if (this.searchType === this.RESOURCE_TYPE) {
				this.selectResource(event);
			}
		},
		
		selectOrder: function(event) {
			var self = this;
			var orderId = $.trim($(event.currentTarget).find('td').parent().attr('data-id'));
			var orderDesc = $.trim($(event.currentTarget).find('td').text());
			self.orderId = orderId;
			self.orderDesc = orderDesc;
			$('#selected-order-tr-id').show();
			$('#selected-order-id').text(orderDesc);
			Utils.forceCloseModal();
		},
		
		selectResource: function(event) {
			var self = this;
			var resourceId = $.trim($(event.currentTarget).find('td').parent().attr('data-id'));
			var resourceDesc = $.trim($(event.currentTarget).find('td').text());
			self.resourceId = resourceId;
			self.resourceDesc = resourceDesc;
			$('#selected-resource-tr-id').show();
			$('#selected-resource-id').text(resourceDesc);
			Utils.forceCloseModal();
		},
		
		addSchedule: function(event) {
			if (this.isValidInputData()) {
				return true;
			}
			else {
				return false;
			}
		},
		
		isValidInputData: function() {
			
			var dateFromTmp = $('#functLogDateFromId').val();
			var dateToTmp = $('#functLogDateToId').val();
			
			var selOrderId = this.orderId;
			var selResourceId = this.resourceId;
			var selOrderDesc = this.orderDesc;
			var selResourceDesc = this.resourceDesc;
			
			if (!selOrderId || selOrderId.length == 0 ||
				!selResourceId || selResourceId.length == 0
			) {
				Utils.openModalError(this, 'Dati non validi:', 'I parametri Attivit&agrave; e Risorsa devono essere valorizzati.');
				return false;
			}
			else if (!dateFromTmp || dateFromTmp.length == 0 ||
				!dateToTmp || dateToTmp.length == 0
			) {
				Utils.openModalError(this, 'Dati non validi:', 'I parametri Data inizio e Data fine devono essere valorizzati.');
				return false;
			}
			else {
				Utils.openModalPopup(this, 'Allocazione completata con successo:', 'La risorsa ' + selResourceDesc + ' &egrave; stata allocata sull\'attivit&agrave; ' + selOrderDesc);
				return true;
			}
		}
		
	});
	
	return SchedulingView;
	
});
