
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/timesheet',
	'../../utils',
	'text!../../templates/timetracker/savedTimesheet.htm'
], function ($, _, Backbone, Timesheet, Utils, SavedTimesheetTpl) {
	
	var ManagementView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
			'keyup #input-text-search' : function (event) {
				Utils.tableSearch(this, event);
			},
			
			'click tr.timesheet-elem' : 'renderContentView'
			
		},
		
		contentView: null,
		timesheet: null,
		timesheetList: null,
		rowList: null,
		sideMenuId: '',
		breadcrum : '',
		
		// TODO: da leggere dal contesto...
		userId: 1,
		
		initialize: function() {
			this.timesheet = new Timesheet();
		},
		
		reset: function(userId) {
//			this.timesheet = new Timesheet({id: userId});
			this.timesheet = new Timesheet();
		},
		
 		render: function (sideMenuId, breadcrum) {
 			this.sideMenuId = sideMenuId;
// 			var userId = 'mrossi'; // Parametro da aggiungere
 			this.reset(); // this.reset(userId);
			var self = this;
			this.breadcrum = breadcrum;
			Utils.spinnerStart();
			
			self.loadItemList();
			
		},
 		
		loadItemList: function () {
			var self = this;
			//	I parametri sono quelli nella sezione "data: $.param..."
			this.timesheet.fetch({
				
				data: $.param({userId: self.userId}),
				
				success: function (collection, response, options) {
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					var respObjs = response;
					self.rowList = respObjs;
					self.renderRows();
					Utils.spinnerStop();
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
//					alert('ERRORE: chiamata ai servizi non andata a buon fine.');
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		renderRows: function() {
			var self = this;
			var tmpl = _.template(SavedTimesheetTpl);
			this.$el.html(tmpl({
									rowList		:	this.rowList,
									breadcrum	: 	self.breadcrum
								})
			);
		},
		
		renderContentView: function(event) {
			var timesheetId = $(event.currentTarget).attr('data-timesheet-id');
			var timesheetUserId = $(event.currentTarget).attr('data-timesheet-user');
			this.contentView.render(this.sideMenuId, timesheetId, timesheetUserId);
		}
		
	});
	
	return ManagementView;
	
});
