
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/order',
	'../../utils',
	'text!../../templates/timetracker/insertOrder.htm'
], function ($, _, Backbone, Order, Utils, InsertOrderTpl) {
	
	var InsertOrderView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
		},
		
		breadcrum : '',
		order : null,
		orderId : '',
		orderDesc : '',
		
		initialize: function() {
			this.order = new Order();
		},
		
		render: function (sideMenuId, breadcrum) {
			var self = this;
			this.breadcrum = breadcrum;
			this.orderId = '';
			this.renderOrder();
		},
 		
		renderOrder: function() {
			var self = this;
			var tmpl = _.template(InsertOrderTpl);
			this.$el.html(tmpl({
									breadcrum	: 	self.breadcrum
								})
			);
		},
		
		isValidInputData: function() {
			
//			var dateFromTmp = $('#functLogDateFromId').val();
//			var dateToTmp = $('#functLogDateToId').val();
//			
//			var selOrderId = this.orderId;
//			var selOrderId = this.orderId;
//			var selOrderDesc = this.orderDesc;
//			var selOrderDesc = this.orderDesc;
//			
//			if (!selOrderId || selOrderId.length == 0 ||
//				!selOrderId || selOrderId.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Attivit&agrave; e Risorsa devono essere valorizzati.');
//				return false;
//			}
//			else if (!dateFromTmp || dateFromTmp.length == 0 ||
//				!dateToTmp || dateToTmp.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Data inizio e Data fine devono essere valorizzati.');
//				return false;
//			}
//			else {
//				Utils.openModalPopup(this, 'Allocazione completata con successo:', 'La risorsa ' + selOrderDesc + ' &egrave; stata allocata sull\'attivit&agrave; ' + selOrderDesc);
//				return true;
//			}
		}
		
	});
	
	return InsertOrderView;
	
});
