
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/resource',
	'../../utils',
	'text!../../templates/timetracker/insertResource.htm'
], function ($, _, Backbone, Resource, Utils, InsertResourceTpl) {
	
	var InsertResourceView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
		},
		
		breadcrum : '',
		resource : null,
		resourceId : '',
		resourceDesc : '',
		
		initialize: function() {
			this.resource = new Resource();
		},
		
		render: function (sideMenuId, breadcrum) {
			var self = this;
			this.breadcrum = breadcrum;
			this.resourceId = '';
			this.renderResource();
		},
 		
		renderResource: function() {
			var self = this;
			var tmpl = _.template(InsertResourceTpl);
			this.$el.html(tmpl({
									breadcrum	: 	self.breadcrum
								})
			);
		},
		
		isValidInputData: function() {
			
//			var dateFromTmp = $('#functLogDateFromId').val();
//			var dateToTmp = $('#functLogDateToId').val();
//			
//			var selOrderId = this.orderId;
//			var selResourceId = this.resourceId;
//			var selOrderDesc = this.orderDesc;
//			var selResourceDesc = this.resourceDesc;
//			
//			if (!selOrderId || selOrderId.length == 0 ||
//				!selResourceId || selResourceId.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Attivit&agrave; e Risorsa devono essere valorizzati.');
//				return false;
//			}
//			else if (!dateFromTmp || dateFromTmp.length == 0 ||
//				!dateToTmp || dateToTmp.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Data inizio e Data fine devono essere valorizzati.');
//				return false;
//			}
//			else {
//				Utils.openModalPopup(this, 'Allocazione completata con successo:', 'La risorsa ' + selResourceDesc + ' &egrave; stata allocata sull\'attivit&agrave; ' + selOrderDesc);
//				return true;
//			}
		}
		
	});
	
	return InsertResourceView;
	
});
