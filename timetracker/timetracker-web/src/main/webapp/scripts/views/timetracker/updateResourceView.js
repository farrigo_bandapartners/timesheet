
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/resource',
	'../../utils',
	'text!../../templates/timetracker/updateResource.htm'
], function ($, _, Backbone, Resource, Utils, UpdateResourceTpl) {
	
	var UpdateResourceView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
		},
		
		breadcrum : '',
		resource : null,
		resourceId : '',
		resourceDesc : '',
		
		initialize: function() {
			this.resource = new Resource();
		},
		
		render: function (breadcrum, resourceId) {
			var self = this;
			this.breadcrum = breadcrum;
			this.resourceId = resourceId;
			Utils.spinnerStart();
			
			this.resource = new Resource({id: resourceId});
			
			this.resource.fetch({
//				data: $.param({sideMenuId: self.sideMenuId,
//								resourceId: self.resourceId}),
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					self.renderResource(response);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
//					alert('ERRORE: chiamata ai servizi non andata a buon fine.');
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
 		
		renderResource: function(user) {
			var self = this;
			var tmpl = _.template(UpdateResourceTpl);
			this.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									resource	: 	user
								})
			);
		},
		
		isValidInputData: function() {
			
//			var dateFromTmp = $('#functLogDateFromId').val();
//			var dateToTmp = $('#functLogDateToId').val();
//			
//			var selOrderId = this.orderId;
//			var selResourceId = this.resourceId;
//			var selOrderDesc = this.orderDesc;
//			var selResourceDesc = this.resourceDesc;
//			
//			if (!selOrderId || selOrderId.length == 0 ||
//				!selResourceId || selResourceId.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Attivit&agrave; e Risorsa devono essere valorizzati.');
//				return false;
//			}
//			else if (!dateFromTmp || dateFromTmp.length == 0 ||
//				!dateToTmp || dateToTmp.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Data inizio e Data fine devono essere valorizzati.');
//				return false;
//			}
//			else {
//				Utils.openModalPopup(this, 'Allocazione completata con successo:', 'La risorsa ' + selResourceDesc + ' &egrave; stata allocata sull\'attivit&agrave; ' + selOrderDesc);
//				return true;
//			}
		}
		
	});
	
	return UpdateResourceView;
	
});
