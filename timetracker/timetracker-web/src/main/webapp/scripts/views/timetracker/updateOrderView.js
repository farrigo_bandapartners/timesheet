
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/order',
	'../../utils',
	'text!../../templates/timetracker/updateOrder.htm'
], function ($, _, Backbone, Order, Utils, UpdateOrderTpl) {
	
	var UpdateOrderView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
		},
		
		breadcrum : '',
		order : null,
		orderId : '',
		orderDesc : '',
		
		initialize: function() {
//			this.order = new Order();
		},
		
		render: function (breadcrum, orderId) {
			var self = this;
			this.breadcrum = breadcrum;
			this.orderId = orderId;
			Utils.spinnerStart();
			
			this.order = new Order({id: orderId});
			
			this.order.fetch({
//				data: $.param({
//								sideMenuId: self.sideMenuId,
//								orderId: self.orderId
//							}),
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
					self.renderOrder(response);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
//					alert('ERRORE: chiamata ai servizi non andata a buon fine.');
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
 		
		renderOrder: function(order) {
			var self = this;
			var tmpl = _.template(UpdateOrderTpl);
			this.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									order		: order
								})
			);
		},
		
		isValidInputData: function() {
			
//			var dateFromTmp = $('#functLogDateFromId').val();
//			var dateToTmp = $('#functLogDateToId').val();
//			
//			var selOrderId = this.orderId;
//			var selResourceId = this.resourceId;
//			var selOrderDesc = this.orderDesc;
//			var selResourceDesc = this.resourceDesc;
//			
//			if (!selOrderId || selOrderId.length == 0 ||
//				!selResourceId || selResourceId.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Attivit&agrave; e Risorsa devono essere valorizzati.');
//				return false;
//			}
//			else if (!dateFromTmp || dateFromTmp.length == 0 ||
//				!dateToTmp || dateToTmp.length == 0
//			) {
//				Utils.openModalError(this, 'Dati non validi:', 'I parametri Data inizio e Data fine devono essere valorizzati.');
//				return false;
//			}
//			else {
//				Utils.openModalPopup(this, 'Allocazione completata con successo:', 'La risorsa ' + selResourceDesc + ' &egrave; stata allocata sull\'attivit&agrave; ' + selOrderDesc);
//				return true;
//			}
		}
		
	});
	
	return UpdateOrderView;
	
});
