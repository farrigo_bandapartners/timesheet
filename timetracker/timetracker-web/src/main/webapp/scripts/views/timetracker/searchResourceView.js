
define([
	'jquery',
	'underscore',
	'backbone',
	'../../models/resource',
	'../../views/timetracker/updateResourceView',
	'../../utils',
	'text!../../templates/timetracker/searchResource.htm'
], function ($, _, Backbone, Resource, UpdateResourceView, Utils, SearchResTpl) {
	
	var SearchResourceView = Backbone.View.extend({
		
		el: $('#container'),
		
		events: {
			
			'keyup #generic-text-search' : function (event) {
				Utils.tableSearch(this, event);
			},
			
			'click tr.resource-search-elem' : function (event) {
				this.loadResource(event);
			}
			
		},
		
		breadcrum : '',
		resource : null,
		updateResourceView : null,
		
		initialize: function() {
			this.resource = new Resource();
			this.updateResourceView = new UpdateResourceView();
		},
		
		render: function (sideMenuId, breadcrum) {
			this.breadcrum = breadcrum;
			var self = this;
			var tmpl = _.template(SearchResTpl);
			this.resource.fetch({
				success: function (collection, response, options) {
					Utils.spinnerStop();
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					self.$el.html(tmpl({
									breadcrum	: 	self.breadcrum,
									title		:	'LISTA RISORSE',
									rowList		: 	response
//									rowList		: 	response.result.dataList
								})
					);
				},
				error: function (collection, response, options) {
					Utils.spinnerStop();
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		},
		
		loadResource: function(event) {
			var resourceId = $(event.currentTarget).attr('id');
			this.updateResourceView.render(this.breadcrum, resourceId);
		}
		
	});
	
	return SearchResourceView;
	
});
