
define([
	'jquery',
	'underscore',
	'backbone',
	'../models/menuItem',
	'./timetracker/contentView',
	'./timetracker/searchTimesheetView',
	'./timetracker/searchAllocationView',
	'./timetracker/searchResourceView',
	'./timetracker/searchOrderView',
	'./timetracker/insertAllocationView',
	'./timetracker/insertResourceView',
	'./timetracker/insertOrderView',
	'./timetracker/searchView',
	'../utils',
	'text!../templates/home.htm'
], function ($, _, Backbone, MenuItem, ContentView, SearchTimesheetView, SearchAllocationView, SearchResourceView,
							SearchOrderView, InsertAllocationView, InsertResourceView, InsertOrderView, SearchView, Utils, HomeTpl) {
	
	var HomeView = Backbone.View.extend({
		
		el: $('#sidebar-wrapper'),
		
		events: {
			'click .menu-item.search-timesheet' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.searchTimesheetView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			'click .menu-item.insert-allocation' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.insertAllocationView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			'click .menu-item.search-allocation' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.searchAllocationView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			'click .menu-item.insert-resource' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.insertResourceView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			'click .menu-item.search-resource' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.searchResourceView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			'click .menu-item.insert-order' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.insertOrderView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			'click .menu-item.search-order' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				this.searchOrderView.render(Utils.getFlowStepName(this, viewItemId), Utils.getBreadcrum(this, viewItemId));
			},
			// Navigazione del menù laterale
			'click .menu-item.navigation' : function(event) {
				event.preventDefault();
				var viewItemId = $(event.currentTarget).attr('data-id');
				Utils.navigateLeftMenu(this, event);
			}
		},
		
		menuItem: null,
		menuItemList: null,
		
		contentView: null,
		searchTimesheetView: null,
		searchAllocationView: null,
		insertAllocationView: null,
		searchResourceView: null,
		insertResourceView: null,
		searchOrderView: null,
		insertOrderView: null,
		searchPopupInsView: null,
		
		initialize: function() {
			this.menuItem = new MenuItem();
			this.contentView = new ContentView();
			this.searchTimesheetView = new SearchTimesheetView();
			this.searchTimesheetView.contentView = this.contentView;
			this.searchAllocationView = new SearchAllocationView();
			this.searchResourceView = new SearchResourceView();
			this.searchOrderView = new SearchOrderView();
			this.insertAllocationView = new InsertAllocationView();
			this.insertResourceView = new InsertResourceView();
			this.insertOrderView = new InsertOrderView();
			this.searchPopupInsView = new SearchView();
			this.searchPopupInsView.parentView = this.insertAllocationView;
			this.insertAllocationView.searchView = this.searchPopupInsView;
		},
		
		render: function () {
			var self = this;
			// TODO: da aggiungere, come parametro, il profilo dell'utente
			this.menuItem.fetch({
				success: function (collection, response, options) {
					
					// Controllo su eventuali errori applicativi
					if (!Utils.checkResponseError(this, response)) {
						return false;
					};
					
//					var menuItemList = response.result.startMenuItems;
					var menuItemList = response;
					self.menuItemList = menuItemList;
					var tpl = _.template(HomeTpl);
					self.$el.html(tpl({	
									menuItemList : menuItemList 
					}));
					
				},
				error: function (collection, response, options) {
					Utils.openModalError(this, 'ERRORE:', 'chiamata ai servizi non andata a buon fine.');
				}
			});
		}
		
	});
	
	return HomeView;
	
});
