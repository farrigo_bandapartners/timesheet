<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">

    <title>BandA Timetracker Home</title>

	<link type="text/css" href="css/gui.css" rel="stylesheet">
	<link type="text/css" href="css/bootstrap/bootstrap.css" rel="stylesheet" />
	<link type="text/css" href="css/bootstrap/bootstrap-datetimepicker.css" rel="stylesheet" />
	<link type="text/css" href="css/bootstrap/simple-sidebar.css" rel="stylesheet" />
	
	<link type="text/css" href="css/jquery-file-upload/jquery.fileupload-noscript.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery-file-upload/jquery.fileupload-ui-noscript.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery-file-upload/jquery.fileupload-ui.css" rel="stylesheet" />
	<link type="text/css" href="css/jquery-file-upload/jquery.fileupload.css" rel="stylesheet" />
	
	<script type="text/javascript" src="js/jquery/jquery-2.2.3.js"></script>
	<script type="text/javascript" src="js/bootstrap/bootstrap-3.3.6.js"></script>
	
	<script data-main="scripts/main" src="<c:out value="${pageContext.servletContext.contextPath}" />/js/require/require-2.2.0.js"></script>
	
	<script type="text/javascript">
	
		var engineRestServiceUrl = "http://localhost:8080/timetracker/rest/entry"; //"${applicationScope['engine-rest-services-url']}";
	
// 		var engineRelativeRestServiceUrl = engineRestServiceUrl.substring(engineRestServiceUrl.indexOf('${pageContext.request.contextPath}'));
	
		var fmwkGuiLoginPage = "${applicationScope['login-page']}";
		
	</script>
	
</head>

<body>

	<jsp:include page="headerBar.jsp" />
	
	<div id="wrapper">
    	<div id="sidebar-wrapper">
		</div>
		<img id="hide-side-menu" title="Chiudi men&#249; laterale" src="img/arrow2.gif" style="cursor: pointer; cursor: hand;">
		<img id="show-side-menu" title="Apri men&#249; laterale" src="img/arrow.gif" style="cursor: pointer; cursor: hand; display: none;">
	    <div id="page-content-wrapper" style="padding-right: 0px;">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-12" style="padding-right: 0px;">
	                	<div id="my-modal-loading-spinner" style="display: none;"></div>
	                	<div id="container">
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>

	<div id="downloadDivId" style="display: none;">
		<a id="downloadAnchorId" href=""></a>
	</div>
	
	<!-- Modal -->
	<div id="fmwk-div-modal-popup" class="modal fade" role="dialog" style="font-size: x-small; font-weight: bolder;">
	 	<div class="modal-dialog" style="width: 900px;">
	    	<!-- Modal content-->
	    	<div id="fmwk-content-modal-popup" class="modal-content fmwk-div-hide">
	      		<div class="modal-header">
			        <button id="fmwk-close-modal-popup" type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 id="fmwk-popup-modal-title" class="modal-title"></h4>
	      		</div>
		      	<div id="fmwk-popup-modal-body" class="modal-body">
		      	</div>
<!-- 	      	<div class="modal-footer"> -->
<!-- 	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
<!-- 	      	</div> -->
	    	</div>
	  	</div>
	</div>
	
	<!-- Modal -->
	<div id="fmwk-div-modal-error" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static" style="font-weight: bold;">
	 	<div class="modal-dialog">
	    	<!-- Modal content-->
	    	<div id="fmwk-content-modal-error" class="modal-content fmwk-div-hide">
	      		<div class="modal-header">
			        <button id="fmwk-close-modal-error" type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 id="fmwk-modal-error-title" class="modal-title"></h4>
	      		</div>
		      	<div id="fmwk-modal-error-body" class="modal-body">
		      	</div>
	    	</div>
	  	</div>
	</div>
	
	<!-- Modal -->
	<div id="fmwk-div-modal-confirm" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static" style="font-weight: bold;">
	 	<div class="modal-dialog">
	    	<!-- Modal content-->
	    	<div id="fmwk-content-modal-confirm" class="modal-content fmwk-div-hide">
	      		<div class="modal-header">
			        <h4 id="fmwk-modal-confirm-title" class="modal-title">Conferma Operazione</h4>
	      		</div>
		      	<div id="fmwk-modal-confirm-body" class="modal-body">
		      	</div>
		      	<div class="modal-footer">
	        		<button type="button" id="btn-modal-confirm-y-id" class="btn btn-default btn-confirm-y" data-dismiss="modal">Si</button>
	        		<button type="button" id="btn-modal-confirm-n-id" class="btn btn-default btn-confirm-n" data-dismiss="modal">No</button>
	        	</div>
	      	</div>
	  	</div>
	</div>
	
	<script type="text/javascript">
	
		$("#hide-side-menu").click(function(e) {
			e.preventDefault();
			$('#hide-side-menu').hide();
			$('#show-side-menu').show();
			$("#wrapper").toggleClass("toggled");
		});
	
		$("#show-side-menu").click(function(e) {
			e.preventDefault();
			$('#show-side-menu').hide();
			$('#hide-side-menu').show();
			$("#wrapper").toggleClass("toggled");
		});
		
	</script>
    
</body>

</html>
